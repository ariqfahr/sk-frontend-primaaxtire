<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" href="<?php echo e(asset('/images/ikon.jpg')); ?>" type="image/gif" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo e(asset('/css/bootstrap.min.css')); ?> ">
   <!--https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css-->
    <link rel="stylesheet" href="<?php echo e(asset('/css/style.css')); ?>">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <link rel="stylesheet" href="<?php echo e(asset('/owlcarousel/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('/owlcarousel/assets/owl.theme.default.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('/css/jquery.typeahead.css')); ?>">


    <title>Primaax Tire | <?php echo $__env->yieldContent('judul_halaman'); ?></title>
  </head>
  <body style="background-color: #0d162c;">

    <style type="text/css">
    


      a {
        text-decoration: none !important;
      }
      .card-product-slider{
        background-color: #2D3B5E;
        border-width: 0px;
        color:#fff;
        border-radius: 15px;
        height: 230;
        overflow: hidden;
      }

      .card-news-slider{
        background-color: #2D3B5E;
        border-width: 0px;
        color:#fff;
        border-radius: 15px;
        height: 300;
        overflow: hidden;
      }

      .card-product-slider img, .card-news-slider img{
        border-top-left-radius: 15px;
        border-top-right-radius: 15px;
      }

      .card-product-slider .card-body .card-desc, .card-news-slider .card-body .card-desc{
        font-size: 13px;
      }

      .card-product-slider .card-body .card-desc-product, .card-news-slider .card-body .card-desc-product {
      
        font-size:1.01vw;
      }

      .img-card-product-slider{
        height: 150px;
        object-fit: cover;
      }


      .btn-owl-slider{
        box-shadow: none !important;
      }

      .btn-slide-carousel{
          border: thin solid #2a385a;
          border-radius: 200px;
          padding:20px 26px;
      }

      .btn-slide-carousel i{
        color: #287cdb  !important;
        font-size:30px;
      }

      .carousel-indicator-dots{
        width: 10px !important;
        height: 10px !important;
        border-radius: 100%;
      }

      .btn-daftar-sekarang{
        background-color: #C39343;
        font-weight: 600;
        color:#fff;
        border-color: #2D344A;
      }

      .bg-navy{
        background-color: #101A33;
      }


      .col-footer-socmed ul{
        padding:0px;
      }

      .col-footer-socmed ul li{
        background-color: transparent !important;
        border: none !important;
        padding:10px;
      }


      .col-footer-socmed ul li a img{

      }



    </style>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primaax py-0">
      <div class="container ">
        <a class="navbar-brand" href="<?php echo e(url('/home/')); ?>">
          <img src="<?php echo e(asset('/images/logo.png')); ?>" width="150" class="d-inline-block align-top" alt="">
         
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerMenu" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

              <?php
                $uri = Request::segment(1);
              ?>

             


        <div class="collapse navbar-collapse" id="navbarTogglerMenu">
          <ul class="navbar-nav navbar-menu mt-lg-0 ml-auto ">
            <li  class="nav-item mx-2 py-2 
                  <?php if($uri == "home" || $uri == ""): ?>
                    <?php
                      echo " active ";
                    ?>
                  <?php endif; ?> 
            ">
              <a class="nav-link" href="<?php echo e(url('/home/')); ?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mx-2 py-2 
                <?php if($uri == "about"): ?>
                    <?php
                      echo " active ";
                    ?>
                 <?php endif; ?> 
            ">
              <a class="nav-link" href="<?php echo e(url('/about/')); ?>">About</a>
            </li>
            <li class="nav-item mx-2 py-2
                <?php if($uri == "product"): ?>
                    <?php
                      echo " active ";
                    ?>
                 <?php endif; ?> 
            ">
              <a class="nav-link" href="<?php echo e(url('/product/')); ?>">Product</a>
            </li>
            <li class="nav-item mx-2 py-2
                <?php if($uri == "partner"): ?>
                    <?php
                      echo " active ";
                    ?>
                 <?php endif; ?> 
            ">
              <a class="nav-link" href="<?php echo e(url('/partner/')); ?>">Partner</a>
            </li>
            <!--
            <li class="nav-item mx-2 py-2
                <?php if($uri == "reseller"): ?>
                    <?php
                      echo " active ";
                    ?>
                 <?php endif; ?> 
            ">
              <a class="nav-link" href="reseller">Reseller</a>
            </li>-->
            <li class="nav-item mx-2 py-2 
                <?php if($uri == "news"): ?>
                    <?php
                      echo " active ";
                    ?>
                 <?php endif; ?> 
            ">
              <a class="nav-link" href="news">News</a>
            </li>
            <li class="nav-item mx-2 py-2 
                <?php if($uri == "contact"): ?>
                    <?php
                      echo " active ";
                    ?>
                 <?php endif; ?> 
            ">
              <a class="nav-link" href="contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

<header>
  <?php echo $__env->yieldContent('head-konten'); ?>
  
</header>

<main>
  <?php echo $__env->yieldContent('konten'); ?>


<div class="col-md-12 bg-navy mb-0"  style="background-color: #0D162C !important">

  <?php if($uri == "home"): ?>
  

    <div class="container py-5">
      <div class="row py-4">
        <div class="col-md-5">
          <center>
            <img src="<?php echo e(asset('/images/reseller.png')); ?>" class="rounded w-90" alt="...">
          </center>
        </div>
        <div class="col-md-7 text-white">
          <h2 class='text-white mt-4'>Become a Reseller & Droshipper</h2>
          <p class="my-4">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>

          <a href="" class="btn btn-warning btn-daftar-sekarang px-4 py-2">
            Daftar Sekarang!
          </a>
        </div>
      </div>
    </div>

  <?php else: ?>
    <br>
    <br>
    <br>
    <br>
    <br>
  <?php endif; ?>

    
  <div class="container pb-5 mb-0">
      <div class="col-md-12 " style="background-color: #2D3B5E;  border-radius: 20px 20px 20px 0px; ">
        <div class="row">
          <div class="col-md-4 pl-0 pt-0 " style="height:295px;">
              <picture  style="position:relative; margin-left: -3px !important; top:-63px !important;">
                <img src="<?php echo e(asset('/images/hp.png')); ?>" class="img-fluid img-thumbnail bg-transparent border-0 " >
              </picture>

          </div>
        
          <div class="col-md-8  text-center align-self-center text-white " style="padding-top:20px;">
            <div class="padding-top: -20px;">
              <p class="h4">
                  Download Aplikasinya sekarang juga !
              </p>
              <div class="d-flex justify-content-center">
              <a href="">
                <img src="<?php echo e(asset('/images/google-play-badge.png')); ?>" style='width:200px;'>
              </a>
            </div>
           
          </div>
          </div>
        </div>
      </div>
  </div>
</div>
</main>


<div class="col-md-12 bg-navy div-gplay mx-0" style="background-color: #2D3B5E">

  <div class="container">
     <footer class="pt-4  pt-md-5 "> <!-- my-md-5 -->
      <div class="row">
        <div class="col-12 col-md-6">
          <img class="mb-2" src="<?php echo e(asset('/images/logo.png')); ?>" alt="" width="150" >
          <ul class="list-unstyled text-small">
            <li><a class="text-muted" href="#">
              Address : Ruko Chofa, Jl. Raya Sukomanunggal Jaya, Sukomanunggal, Kec. Sukomanunggal, Kota SBY, Jawa Timur 60188
            </a></li>
            <li><a class="text-muted" href="#">
              Hotline : (031) 7326835
            </a></li>
            <li><a class="text-muted" href="#">
              Monday-Friday: 08:00 am - 05.00 pm
            </a></li>
          </ul>
        </div>
        <div class="col-6 col-md-3">
          <h5 class='text-white'>Menu</h5>
          <ul class="list-unstyled text-small" style="columns: 2; -webkit-columns: 2; -moz-columns: 2;">
            <li><a class="text-muted" href="<?php echo e(url('/home/')); ?>">Home</a></li>
            <li><a class="text-muted" href="<?php echo e(url('/about/')); ?>">About</a></li>
            <li><a class="text-muted" href="<?php echo e(url('/product/')); ?>">Product</a></li>
            <li><a class="text-muted" href="<?php echo e(url('/partner/')); ?>">Partner</a></li>
            <li><a class="text-muted" href="<?php echo e(url('/news/')); ?>">News</a></li>
            <li><a class="text-muted" href="<?php echo e(url('/contact/')); ?>">Contacts</a></li>
          </ul>
        </div>
        <div class="col-6 col-md-3 col-footer-socmed">
          <h5 class='text-white'>Follow Us</h5>
          <ul class="list-group list-group-horizontal">
             <?php $__currentLoopData = $sosmed->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php 
                    $id_sosmed          = $value->id;
                    $name_sosmed        = $value->name;
                    $url_sosmed         = $value->url;
                    $gambar_sosmed      = $value->image;


                  ?>


            <li class="list-group-item ">
              <a href="<?php echo e($url_sosmed); ?>" target="_blank">
                <img src="<?php echo e($gambar_sosmed); ?>" class="img-fluid">
              </a>
            </li>


            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          
          </ul>
        </div>
       
      </div>
    </footer>
  </div>


  <hr>

    <div class="container">
      <div class="row">
        <div class="col-md-9 ">
            <p class="d-block mb-3 text-muted">© 2019 PRIMAAX. All Rights Reserved</p>
        </div>
        <div class="col-md-3 ml-auto">

          <div class="row">
            <div class="col">
              <a href="">Privacy Policy</a>
            </div>
            <div class="col">
              <a href="">Refund Policy</a>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" ></script>
    <!--<script src="<?php echo e(asset('/js/jquery-3.3.1.slim.min.js')); ?>" ></script>-->
    <script src="<?php echo e(asset('/js/popper.min.js')); ?> " ></script>
    <script src="<?php echo e(asset('/js/bootstrap.min.js')); ?> "></script>


    <script src="<?php echo e(asset('/owlcarousel/owl.carousel.js')); ?> "></script>

    <script>
            $(document).ready(function() {


              var owl_our_news = $("#owl-our-news");
              owl_our_news.owlCarousel({
                  responsiveClass:true,
                  responsive:{
                      0:{
                          items:1,
                          nav:false,
                          dots:false
                      },
                      600:{
                          items:2,
                          nav:false,
                          dots:false
                      },
                      1000:{
                          items:2,
                          nav:false,
                          loop:false,
                          dots:false
                      }
                  }
              });

               // Custom Navigation Events
              $(".next-news").click(function(){
                owl_our_news.trigger('next.owl.carousel');
              })
              $(".prev-news").click(function(){
                owl_our_news.trigger('prev.owl.carousel');
              })




              var owl = $("#owl-demo");

              owl.owlCarousel({
                  responsiveClass:true,
                  responsive:{
                      0:{
                          items:1,
                          nav:false,
                          dots:false
                      },
                      600:{
                          items:3,
                          nav:false,
                          dots:false
                      },
                      1000:{
                          items:5,
                          nav:false,
                          loop:false,
                          dots:false
                      }
                  }
              });




             
              // Custom Navigation Events
              $(".next-product").click(function(){
                owl.trigger('next.owl.carousel');
              })
              $(".prev-product").click(function(){
                owl.trigger('prev.owl.carousel');
              })
              $(".play").click(function(){
                owl.trigger('play.owl.carousel',1000); //owl.play event accept autoPlay speed as second parameter
              })
              $(".stop").click(function(){
                owl.trigger('stopss.owl.carousel');
              })

            })
          </script>

    <script type="text/javascript">
      $(document).ready(function(){
        // Activate Carousel
        $("#our-product").carousel({interval: false});
      });
    
    </script>


    <?php echo $__env->yieldContent('js-konten'); ?>


  </body>
</html>