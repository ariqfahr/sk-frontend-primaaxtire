<?php $__env->startSection('judul_halaman', 'News'); ?>

<?php $__env->startSection('head-konten'); ?>
<style type="text/css">
	.bg-news-page{
		background-image: url("<?php echo e(asset("/images/banner inner page.png")); ?>");
		background-size: cover;
	}

	.breadcrumb-news {
		background:none !important;
	}

	.breadcrumb-item-news {
		color:#fff !important;
	}

	.breadcrumb-item-news a{
		color:#fff !important;
	}

	.breadcrumb-item-news+.breadcrumb-item-news::before{
		content: ">" !important;
		color:#fff !important;

	}

	.list-group-product a, .list-group-product a:focus{
		background-color: #2D3B5E !important;
		color:#fff;
		font-weight: bold;
	}


	.list-group-product a:hover{
		color: aqua !important;
	}


	.list-group-product-jr li{
		background-color: #2D3B5E !important;
		padding-left:50px !important; 
		font-size:13px;
		color:#fff !important;
		border: none !important;
	}


	.list-group-product-jr a{
		text-decoration: none !important;
	}

	.list-group-product-jr a:hover, .list-group-product-jr a:focus{
		color: #395B98 !important;
	}


	.accordion-toggle:after {
	    /* symbol for "opening" panels */
	    font-family:'FontAwesome';
	    content:"\f106";
	    float: right;
	    color: inherit;
	}

	.collapsed .accordion-toggle:after {
	    /* symbol for "collapsed" panels */
	    content:"\f107";
	}

	.map-container-2{
		overflow:hidden;
		padding-bottom:56.25%;
		position:relative;
		height:0;
	}
	.map-container-2 iframe{
		left:0;
		top:0;
		height:100%;
		width:100%;
		position:absolute;
	}

	.btn-lokasi{
		background-color: #C39343;
		border:none !important;
		color:#fff !important;
	}

	.form-partner{
		background-color: transparent !important;
		border:1px solid #fff !important;
		border-radius: 5px !important;
	}

	.form-partner-group{
		background-color: transparent !important;
		border:1px solid #fff !important;
		border-radius: 5px !important;
		border-top-right-radius: 0 !important;
    	border-bottom-right-radius: 0 !important;
    	border-right: none !important;
	}

	.button-partner-group{
		background-color: transparent !important;
		border-top-left-radius: 0;
    	border-bottom-left-radius: 0;
    	border-left: none !important;
    	color: #fff !important;
	}


	#infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
       display: none;
    }

    #map-canvas #infowindow-content {
        display: inline;
    }
	

</style>

<div class="col-md-12 bg-news-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning" style="color:#c39343 !important;">Partner</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('konten'); ?>


<div class="col-md-12 "  style="border-bottom: thin solid  aqua">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-news mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-news"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-news text-capitalize active" aria-current="page">
		    	<?php echo e(Request::segment(1)); ?>

		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="container mb-5">

	<h4 class="text-white text-left text-sm-left mt-4 text-capitalize">
    	<b> <?php echo e(Request::segment(1)); ?> </b>
    </h4>
    
   
    <?php
    /*
    <form>
        <!--<div class="typeahead__container">
            <div class="typeahead__field">
                <div class="typeahead__query">
                    <input 
                           name="q"
                           type="search"
                           autofocus
                           autocomplete="off">
                </div>
                <div class="typeahead__button">
                    <button type="submit">
                        <span class="typeahead__search-icon"></span>
                    </button>
                </div>
            </div>
        </div>-->
    </form>
	*/
	?>

    <div class="row ">
		<div class="col-md-3 pt-2 text-center">
			<a class="btn btn-warning btn-lokasi btn-lg px-4 py-3 w-100" style="height:65px;  "
			onclick="initMapByNear()"; 
			>
				Lokasi Terdekat Saya
			</a>
		</div>
		
		<div class="col-md-3 pt-2 ">
			<div class="typeahead__container">
            <div class="typeahead__field">
                <div > <!-- class="typeahead__query" --> 
                    <input class="form-control form-control-lg form-partner  px-4 js-typeahead"
                           name="q"
                           type="search"
                           autofocus
                           autocomplete="off"
                            placeholder="Cari Berdasarkan Kota" style="height:65px;"  >
                </div>
            </div>
           	</div>

           	 <div class="js-result-container"></div>

		</div>

  		<div class="col-md-4 pt-2 ml-auto">
  			<div class="input-group mb-3">
  				<div class="typeahead__container">
            		<div class="typeahead__field">

					  <input type="text" class="form-control form-partner-group" placeholder="Cari nama toko" aria-label="Recipient's username" aria-describedby="basic-addon2" style="height:65px; " id="typeahead-stores">
					  <div class="input-group-append">
					    <button class="btn btn-outline-light button-partner-group" type="button" onclick="initMapByName()">
					    	<i class="fas fa-search"></i>
					    </button>
					 </div>
            		</div>
			  </div>
			</div>
  		</div>


					
	</div>

	<div class="row m-0 pt-4">

		<?php $__currentLoopData = $lokasi->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php 
                $id         = $val->id;
                $nama     	= $val->name;
                $latitude   = $val->latitude;
                $longitude  = $val->longitude;
        	 ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

		<input type="hidden" name="id" id="idMaps" value="<?php echo $id ?>">
		<input type="hidden" name="nama" id="namaMaps" value="<?php echo $nama ?>">
		<input type="hidden" name="lat" id="latlng" value="<?php echo $latitude.",".$longitude ?>">

			<?php
				if(!empty($_GET["searchBy"])){
					$page_id = $_GET["searchBy"] ;
				} else{

		    		$page_id = 0;
				}
			?>
			<!--Google map-->
			<div class="col-sm-12 col-md-12 col-lg-12 z-depth-1-half map-container-2" style="height: 500px" id="map-canvas">
			  <!--<iframe class="" src="https://maps.google.com/maps?q=chicago&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>-->
			</div>

			<div id="infowindow-content">
		      <img src="" width="16" height="16" id="place-icon">
		      <span id="place-name"  class="title"></span><br>
		      <span id="place-address"></span>
		    </div>

			<!--Google Maps-->

	</div>

			
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('js-konten'); ?>



<script>
	var marker;

	var map;

	function initMap() {  

		if (navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(showPositionByNear);
		} else { 
		    x.innerHTML = "Geolocation is not supported by this browser.";
		}

	}

    function initMapByNear() {   

		if (navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(showPositionByNear);
		} else { 
		    x.innerHTML = "Geolocation is not supported by this browser.";
		}
		
    }

    function showPositionByNear(position) {
		  	var latitude = position.coords.latitude;
			var longitude= position.coords.longitude;

			console.log(latitude);
			console.log(longitude);

		  	//var input = document.getElementById('searchCities').value;

	    	var route = "<?php echo e(url('/partner/get-by-near')); ?>";
	    	var url = route+'?latitude='+latitude+'&longitude='+longitude;

	    	$.getJSON(url, callbackJsonByNear);

	    	var id = [];
	    	var label = [];
	    	var lati = [];
	    	var longi = [];

	    	var dataMulti =  [];

	    	var idMulti  = 0;

	    	function callbackJsonByNear(json) {
	    		var idMultiDua  = 0;

	    		console.log(json.kode);
	    			$.each(JSON.parse(json), function(i, field){
						dataMulti.push([field.id, field.name, field.latitude, field.longitude]);
						longi.push(field.longitude);
						lati.push(field.latitude);
				    	idMultiDua++;
				    });

				    idMulti++;
					console.log(dataMulti);
					setGmapsByNear(map);


	    	}

	    	


	    	function setGmapsByNear(map) {

	    		var map = new google.maps.Map(document.getElementById('map-canvas'), {
				    zoom: 13,
				    center: {lat: dataMulti[0][2], lng: dataMulti[0][3]}
				});

	    		for (var i = 0; i < dataMulti.length; i++) {
				    var beach = dataMulti[i];
				    var marker = new google.maps.Marker({
				      position: {lat: beach[2], lng: beach[3]},
				      map: map,
				      title: beach[1],
				      label: beach[1]
				    });
				  }
	    	}
		}       

    function initMapByName() {   

		var input = document.getElementById('typeahead-stores').value;

    	var route = "<?php echo e(url('/partner/get-by-name')); ?>";
    	var url = route+'?store='+input;

    	$.getJSON(url, callbackJsonByName);

    	var id;
    	var label;
    	var lati;
    	var longi;

    	function callbackJsonByName(json) {

    		
    		var keys = Object.keys(json);

    		var jsonObject = JSON.parse(json);

    		//console.log(jsonObject);

    		/*keys.forEach(function(item, index) {
    			console.log(index, item.kode);
			});*/
			//console.log(JSON.parse(json));

			var kode =  jsonObject.status.kode;

			console.log(kode);

			if (kode==200) {
				console.log(jsonObject.value.length);


				$.each(jsonObject.value, function(i, field){
    				//console.log(field.id);
					id  = field.id;
	    		  	longi = field.longitude;
	    		 	lati = field.latitude;
	    		 	label = field.name;


			    });

		
	    		setGmaps(); 
			} else  {
				alert("Maaf, data tidak ditemukan. ");
			}

    		
    	}


    	function setGmaps() {

    		var latlngStr = input.split(',', 2);
			var latlng = new google.maps.LatLng(lati, longi);

			var infowindow = new google.maps.InfoWindow();
	        var infowindowContent = document.getElementById('infowindow-content');
	        infowindow.setContent(infowindowContent);
	        
			var myLatLng = {lat: lati, lng: longi};

	        var Lating = new google.maps.LatLng(lati, longi),
	            options = {
	            	center: Lating,
	            	zoom: 12
	            },
	        
	        map = new google.maps.Map(document.getElementById("map-canvas"), options);
			
			var marker = new google.maps.Marker({
	              position: Lating,
	              map: map,
				  anchorPoint: new google.maps.Point(0, -29),
				  title: label,
	              label: label
	        });   
    	}

		         
    }


   

    function initMapByAuto(input) {   
    	//alert (id);
		//var input = document.getElementById('searchCities').value;

    	var route = "<?php echo e(url('/partner/get-by-auto')); ?>";
    	var url = route+'?city='+input;

    	$.getJSON(url, callbackJsonByName);

    	var id = [];
    	var label = [];
    	var lati = [];
    	var longi = [];

		var dataMulti =  [];

    	var idMulti  = 0;

    	function callbackJsonByName(json) {

    		console.log(json);

    		var idMultiDua  = 0;
    		$.each(JSON.parse(json), function(i, field){

				dataMulti.push([field.id, field.name, field.latitude, field.longitude]);
				longi.push(field.longitude);
				lati.push(field.latitude);

    			/*console.log(idMultiDua);

				dataMulti[idMultiDua][0] = field.id;
				dataMulti[idMultiDua][1] = field.name;*/

				/*dataMulti[idMulti].push(field.name);
				dataMulti[idMulti].push(field.atitude);
				dataMulti[idMulti].push(field.longitude);
				longi.push(field.longitude);
				lati.push(field.latitude);
				label.push(field.name);*/

    		  	/*longi[] = field.longitude;
    		 	lati[] = field.latitude;
    		 	label[] = field.name;*/

		    	idMultiDua++;
		    });

		    idMulti++;


    		console.log(dataMulti);

			//alert(id.length);

			

			//setMarkers(map);

    		 

    		 setGmapslagi(map);
    	}

    	


    	function setGmapslagi(map) {

    		var map = new google.maps.Map(document.getElementById('map-canvas'), {
			    zoom: 13,
			    center: {lat: dataMulti[0][2], lng: dataMulti[0][3]}
			});

    		for (var i = 0; i < dataMulti.length; i++) {
			    var beach = dataMulti[i];
			    var marker = new google.maps.Marker({
			      position: {lat: beach[2], lng: beach[3]},
			      map: map,
			      title: beach[1],
			      label: beach[1]
			    });
			  }
    	}

    	function setGmaps() {

    		var i;

				var Lating = [];

			for (i = 0; i < id.length; i++) { 
			  	var latlngStr = input.split(',', 2);
				var latlng = new google.maps.LatLng(lati[i], longi[i]);

				var infowindow = new google.maps.InfoWindow();
		        var infowindowContent = document.getElementById('infowindow-content');
		        infowindow.setContent(infowindowContent);
		        


				var myLatLng = [];
				myLatLng.push({lat: lati[i], lng: longi[i]});

				var newLabel = [];
				newLabel.push(label[i]);


		        Lating[i] = new google.maps.LatLng(lati[i], longi[i]),
		            options = {
		            	center: Lating[i],
		            	zoom: 15
		            },
		        
		        map = new google.maps.Map(document.getElementById("map-canvas"), options);


		        for (a = 0; a < newLabel.length; a++) { 


		        	console.log(newLabel[a]);
		        	console.log(myLatLng[a]);

		        	var marker = new google.maps.Marker({
		              position: myLatLng[a],
		              map: map,
					  anchorPoint: new google.maps.Point(0, -29),
					  title: newLabel[a],
		              label: newLabel[a]
		            });

		        	
		        	/*var marker = new google.maps.Marker({
				      position: myLatLng[a],
				      map: map,
				      icon: image,
				      shape: shape,
				      title: beach[0],
				      zIndex: beach[3]
				    });*/

				    /*var beach = newLabel[a];

		        	var marker = new google.maps.Marker({
		              position: myLatLng[a],
		              map: map,
					  anchorPoint: new google.maps.Point(0, -29),
					  title: newLabel[a],
		              label: newLabel[a]
		            });*/   
		        }


			}



    		
			
			
    	}

		         
        }



    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('searchCities');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
 
	// kita munculkan peta default 
	/*function initialize() {


  		map = new google.maps.Map(document.getElementById('map-canvas'), {
        	zoom: 12,
        	// posisi default ada di kota Bandung
        	center: {lat: -7.273843, lng: longitude}
      	});
	}*/
 
	/*function codeLatLng() {
		// ambil value dari combobox


   

		var input = document.getElementById('latlng').value;

		var latlngStr = input.split(',', 2);
		var latlng = new google.maps.LatLng(latlngStr[0], latlngStr[1]);


		var myLatLng = {lat: latlngStr[0], lng: latlngStr[1]};
            
        map = new google.maps.Map(document.getElementById('map'), {
              center: myLatLng,
              zoom: 14                    
            });

		// cari lokasi dari latitude dan longitude
		geocoder.geocode({'location': map}, function(results, status) {

			if (status == google.maps.GeocoderStatus.OK) {
				;
				// jika berhasil, map akan secara automatis berpindah ke koordinat tersebut
				if (results[1]) {
			    	map.setZoom(13);
			    	map.setCenter(map)
			    	marker = new google.maps.Marker({
			      		position: map,
			      		map: map
			    	});
			    	infowindow.setContent(results[1].formatted_address);
			    	infowindow.open(map, marker);

			  	} else {
			    	window.alert('No results found');
			  	}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
	}
 
	google.maps.event.addDomListener(window, 'load', initialize);*/
    </script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCv200mEGfbuiFnGWWXRvi8UATWwzszGu0&callback=initMap&libraries=places&region=ID" 
type="text/javascript"></script>
 <script src="<?php echo e(asset('/js/jquery.typeahead.js')); ?>"></script>

  <script>

        var data = [{id: 1, display:"Afghanistan"}, {id: 3, display:"Albania"}];


    	var routes     = "<?php echo e(url('/partner/get-kota')); ?>";
    	var routesToko = "<?php echo e(url('/partner/get-nama')); ?>";


        var dataKota = [];
        var dataToko = [];

    	
		$(document).ready(function(){  

			$.getJSON(routes, callbackJsonCity);
	    	
	    	function callbackJsonCity(json) {
	    		$.each(json, function(i, field){
	    		  var jsonId = {id: field.id, kota: field.kota};
			      dataKota.push(jsonId);
			    });

	    		fungsiTypeaheadKota(); 
	    	}

	    	function fungsiTypeaheadKota() {
			    //console.log(dataKota);

	    		typeof $.typeahead === 'function' && $.typeahead({
	            input: ".js-typeahead",
	            minLength: 1,
	            order: "asc",
	            //group: true,
	            maxItemPerGroup: 3,
	            /*groupOrder: function (node, query, result, resultCount, resultCountPerGroup) {

	                var scope = this,
	                    sortGroup = [];

	                for (var i in result) {
	                    sortGroup.push({
	                        group: i,
	                        length: result[i].length
	                    });
	                }

	                sortGroup.sort(
	                    scope.helper.sort(
	                        ["length"],
	                        false, // false = desc, the most results on top
	                        function (a) {
	                            return a.toString().toUpperCase()
	                        }
	                    )
	                );

	                return $.map(sortGroup, function (val, i) {
	                    return val.group;
	                });
	            },*/
	            hint: true,
	            display: ["id", "kota"],

	            dropdownFilter: "All",
	            //href: "https://en.wikipedia.org/?title=",
	            template: "<div id='{{id}}'  > {{kota}}</div> ",
	            emptyTemplate: "no result for {{query}} ",
	            source: {
	                    data: dataKota
	                
	            },
	            callback: {
	                onClickAfter: function (node, a, item, event) {
						//console.log(node, a, item, event);
						//console.log(item.id);
	                    initMapByAuto(item.kota);
	                    $('.js-result-container').text('');

	                },
	                onResult: function (node, query, obj, objCount) {

	                    console.log(objCount)

	                    var text = "";
	                    if (query !== "") {
	                        text = objCount + ' elements matching "' + query + '"';
	                    }
	                    $('.js-result-container').text(text);

	                }
	            },
	            debug: true
	        });
	    	}

	    	$.getJSON(routesToko, callbackJsonStores);

	    	function callbackJsonStores(json) {

	    		$.each(json, function(i, field){
	    		  var jsonId = {id: field.id, toko: field.name};
			      dataToko.push(jsonId);
			    });

	    		fungsiTypeaheadStores(); 
	    	}

	    	function fungsiTypeaheadStores() {
			    console.log(dataToko);
	    		typeof $.typeahead === 'function' && $.typeahead({
	            input: "#typeahead-stores",
	            minLength: 1,
	            order: "asc",
	            maxItemPerGroup: 3,
	            hint: true,
	            display: ["id", "toko"],
	            dropdownFilter: "All",
	            template: "<div id='{{id}}'  > {{toko}}</div> ",
	            emptyTemplate: "no result for {{query}} ",
	            source: {
	                    data: dataToko
	            },
	            callback: {
	                /*onClickAfter: function (node, a, item, event) {
						initMapByStores(item.name);
	               		$('.js-result-container').text('');

	                },*/
	                onResult: function (node, query, obj, objCount) {

	                    console.log(objCount)

	                    var text = "";
	                    if (query !== "") {
	                        text = objCount + ' elements matching "' + query + '"';
	                    }
	                    $('.js-result-container').text(text);

	                }
	            },
	            debug: true
	        });
	    	}




    	

		//console.log(data);

		

		});



        

    </script>

 
<script type="text/javascript">
    var route = "<?php echo e(url('/partner/get-kota')); ?>";
    var $input = $("#searchCities");


    $input.typeahead({
	   source:  function (term, process) {
    	
        return $.get(route, 
        	{ term: term }, 
        		function (data) {
        			//console.log(data);
					//alert(JSON.stringify(data));
                	return process(data);
            	}
            );
        } ,
	  autoSelect: true
	});

    
	$input.change(function() {
	  var current = $input.typeahead("getActive");
	  if (current) {
	    // Some item from your model is active!
	    if (current.name == $input.val()) {
	    	alert("klik");
	      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
	    } else {
	    	//alert("ga ada");
	      // This means it is only a partial match, you can either add a new item
	      // or take the active if you don't want new items
	    }
	  } else {
	    // Nothing is active so it is a new value (or maybe empty value)
	  }
	});

	/* untuk source di atas
	$('#q').typeahead({
        source:  function (term, process) {
    	
        return $.get(route, 
        	{ term: term }, 
        		function (data) {
        			//console.log(data);
					//alert(JSON.stringify(data));
                	return process(data);
            	}
            );
        }
    });*/
  
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>