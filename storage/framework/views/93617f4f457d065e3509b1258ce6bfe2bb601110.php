<?php $__env->startSection('judul_halaman', 'News'); ?>

<?php $__env->startSection('head-konten'); ?>
<style type="text/css">
	.bg-news-page{
		background-image: url("<?php echo e(asset("/images/banner inner page.png")); ?>");
		background-size: cover;
	}

	.breadcrumb-news {
		background:none !important;
	}

	.breadcrumb-item-news {
		color:#fff !important;
	}

	.breadcrumb-item-news a{
		color:#fff !important;
	}

	.breadcrumb-item-news+.breadcrumb-item-news::before{
		content: ">" !important;
		color:#fff !important;

	}
</style>

<div class="col-md-12 bg-news-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning" style="color:#c39343 !important;">NEWS</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('konten'); ?>


<div class="col-md-12 "  style="border-bottom: thin solid  #202e51">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-news mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-news"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-news text-capitalize active" aria-current="page">
		    	<?php echo e(Request::segment(1)); ?>

		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="container">
	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12 p-0">
			<div class="row m-0">
				<?php $__currentLoopData = $news->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $berita): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php 
                        $id         = $berita->id;
                        $judul      = $berita->title;
                        $tanggal    = $berita->date_created;
                        $deskripsi  = $berita->description;
                        $pembuat    = $berita->user_created;
                        $gambar     = $berita->image;
                 ?>


				    <div class="col-6  col-sm-6 col-md-6 mt-4">
		                <div class="card card-news-slider">
		                    <img class="card-img-top img-card-product-slider img-fluid" src="<?php echo e($gambar); ?>" alt="Card image cap">
		                    <div class="card-body">
		                        <p class="card-text mb-2"><small class="text-muted"> <?php echo e(Carbon\Carbon::parse($tanggal)->formatLocalized('%d %B %Y')); ?> | <?php echo e($pembuat); ?></small></p>
		                        <h5 class="card-title mt-0"><?php echo e($judul); ?></h5>
		                            <p class="card-text card-desc">
		                                <?php echo e($deskripsi); ?>

				 	                </p>
		                    </div>
		                </div>
		            </div>


				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

				<div class="col-md-12 mt-4">
					
				<div class="float-right text-white">

					<nav aria-label="Page navigation example">
					  <ul class="pagination justify-content-end">

					  	<?php
			             	if(!empty($_GET["page"]) && ($_GET["page"] != 0)){
								$prev_id = $_GET["page"] - 1;
								$page_now = $_GET["page"];
								$next_id  = $page_now + 1;

							} else if (!empty($_GET["page"]) && ($_GET["page"] == 0)) {
								$prev_id  = $_GET["page"];
								$page_now = $_GET["page"] + 1;
								$next_id  = $page_now + 1;
							}
							else{
					    		$prev_id = 0;
								$page_now = 1;
								$next_id  = $page_now + 1;

								$css_disable = " disabled ";
							}
			            ?>


					   	<?php if($news->pages > 1): ?> 

					    <li class="page-item 
					    	<?php 
					    		echo $css_disable
					    	 ?>
					    	"
					    >
					      <a class="page-link" 
					      	href="<?php echo e(url('/news?page=$prev_id')); ?>" 

					      	<?php 
					    		echo $css_disable
					    	 ?>

					      tabindex="-1">Previous</a>
					    </li>

						<?php endif; ?> 


					   	<?php if($news->pages > 1): ?>
						<?php for($i = 1; $i <= $news->pages; $i++): ?>
							<li class="page-item 
								<?php if($i == $page_now): ?>
									active
								<?php endif; ?> 
							">
								<a class="page-link" href="#">
									<?php echo e($i); ?>

								</a>
							</li>

						<?php endfor; ?>
						
						<?php endif; ?> 

					   	<?php if($news->pages > 1): ?> 
							<li class="page-item">
						      <a class="page-link" href="#">Next</a>
						    </li>
						<?php endif; ?> 

					    
					  </ul>
					</nav>
					

				</div><br>

				</div>
				


			</div>
		</div>
	</div>

			
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>