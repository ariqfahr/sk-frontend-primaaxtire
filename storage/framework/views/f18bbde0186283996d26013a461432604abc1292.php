<?php $__env->startSection('judul_halaman', 'About'); ?>

<?php $__env->startSection('head-konten'); ?>
<style type="text/css">
	.bg-news-page{
		background-image: url("<?php echo e(asset('/images/sample-gambar1.jpg')); ?>");
		background-size: cover;
	}

	.breadcrumb-news {
		background:none !important;
	}

	.breadcrumb-item-news {
		color:#fff !important;
	}

	.breadcrumb-item-news a{
		color:#fff !important;
	}

	.breadcrumb-item-news+.breadcrumb-item-news::before{
		content: ">" !important;
		color:#fff !important;

	}
</style>


<div class="col-md-12 bg-news-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning" style="color:#c39343 !important;">About</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('konten'); ?>


<div class="col-md-12 bg-navy"  style="border-bottom: thin solid  aqua">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-news mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-news"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-news text-capitalize active" aria-current="page">
		    	<?php echo e(Request::segment(1)); ?>

		    </li>
		  </ol>
		</nav>
	</div>
</div>



<?php $__currentLoopData = $about->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      
        	<?php if($value->id == "2"): ?> 
        	  	<?php 
		          $id_about         = $value->id;
		          $judul_about       = $value->title;
		          $deskripsi_about   = $value->description;
		          $gambar_about      = $value->image;

         		 ?>

	        <?php elseif($value->id == "3"): ?> 
	        	<?php 
		        
		        	$id_mission        = $value->id;
			        $judul_mission       = $value->title;
			        $deskripsi_mission   = $value->description;
			        $gambar_mission      = $value->image;
			     ?>
		    <?php else: ?>
		    	<?php 
		        	$id_goals       = $value->id;
			        $judul_goals      = $value->title;
			        $deskripsi_goals  = $value->description;
			        $gambar_goals     = $value->image;

		         ?>
		        
	        <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



<div class="col-md-12 bg-navy">
<div class="container">
	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12">
			<h4 class="text-white my-4">
				<b> About us </b>
			</h4>
			<div class="row">
			    <div class="col-sm-6 order-sm-12 mb-4">
			      <img src="<?php echo e($gambar_about); ?>" alt="" class="rounded w-100"  />
			    </div>
			    <div class="col-sm-6 order-sm-1 text-white">
			      	<p>
			      		<?php echo e($deskripsi_about); ?>

					</p>
			    </div>
			  </div>

			
		</div>
	</div>

</div>

</div>

<div class="col-md-12 ">
    <div class="container py-4">

		<div class="row">
			<div class="col-md-6 text-white ">
				<h3 class="my-4">Our Goals</h3>
				<p>
		        		<?php echo e($deskripsi_goals); ?>

		        		<br>
		        </p>
			</div>

			<div class="col-md-6 text-white ">

				<h3 class="my-4">Our Mission</h3>
				<p>
		        		
		        		<?php echo e($deskripsi_mission); ?>

		        		<br>
		        </p>
			</div>

		</div>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>