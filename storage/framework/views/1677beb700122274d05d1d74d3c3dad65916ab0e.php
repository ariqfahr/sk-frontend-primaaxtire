<?php $__env->startSection('judul_halaman', 'Home'); ?>

<?php $__env->startSection('head-konten'); ?>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">

      <?php 
        $urutan = 0;    
       ?>

      <?php $__currentLoopData = $slider->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <li data-target="#carouselExampleIndicators" 
          data-slide-to="<?php echo e($urutan++); ?>" 
        class="carousel-indicator-dots active">
         
        </li>

        

      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <!--<li data-target="#carouselExampleIndicators" data-slide-to="1" class="carousel-indicator-dots" ></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2" class="carousel-indicator-dots" ></li>-->
    </ol>
    <div class="carousel-inner">

      <?php $__currentLoopData = $slider->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          

      <div class="carousel-item item-background active" >
          <img src="<?php echo e($slide->image); ?>"  class="d-block w-100">
          <?php  
            /*<div class="carousel-caption d-none d-md-block">
              <h5>


              </h5>
              <p>...</p>
            </div>*/

          ?>

      </div>


      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

     <!-- <div class="carousel-item item-background"  >
          <img src="<?php echo e(asset('/images/slide2.jpg')); ?>"  class="d-block w-100">
      </div>
      <div class="carousel-item item-background">
          <img src="<?php echo e(asset('/images/slide3.jpg')); ?>"  class="d-block w-100">
      </div>-->
    </div>


    <?php
    /*<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>*/
    ?>
  </div>

  <div class="col-md-12 bg-navy py-2">
    <div class="d-flex justify-content-center">
      <a href="">
        <img src="<?php echo e(asset('/images/google-play-badge.png')); ?>" style='width:200px;'>
      </a>
    </div>
  </div>
  
<?php $__env->stopSection(); ?>


<?php $__env->startSection('konten'); ?>
<main>
<div class="col-md-12">
  <div class="container">
    <div class="row py-4">

      <?php $__currentLoopData = $about->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($value->id == "2"): ?> 
              <?php 
              $id_about         = $value->id;
              $judul_about       = $value->title;
              $deskripsi_about   = $value->description;
              $gambar_about      = $value->image;

             ?>

          <?php elseif($value->id == "3"): ?> 
            <?php 
            
              $id_mission        = $value->id;
              $judul_mission       = $value->title;
              $deskripsi_mission   = $value->description;
              $gambar_mission      = $value->image;
           ?>
        <?php else: ?>
          <?php 
              $id_goals         = $value->id;
              $judul_goals      = $value->title;
              $deskripsi_goals  = $value->description;
              $gambar_goals     = $value->image;

             ?>
            
          <?php endif; ?>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


      <div class="col-md-6">
        <img src="<?php echo e($gambar_about); ?>" class="rounded float-left w-100" alt="...">
      </div>
      <div class="col-md-6 text-white">
        <h2 class='text-white'><?php echo e($judul_about); ?></h2>
        <p class="my-4">
          <?php echo e($deskripsi_about); ?>

        </p>

        <a href="<?php echo e(url('/about/')); ?>"class="h5 text-info" style="text-decoration: none !important;">
          Read More
        </a>
      </div>
    </div>
  </div>
</div>
  
  <div class="col-md-12 bg-navy div-gplay">
    <div class="container-fluid py-4">
        <h3 class="text-white text-center mb-4">Our Product</h3>


        <div class="carousel slide " data-ride="carousel">
         
          <div class="carousel-inner">
            <div class="col-md-10 offset-md-1 px-5">
                  <div id="owl-demo" class="owl-carousel owl-theme">
                    <?php $__currentLoopData = $product->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $produk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php 
                        $id         = $produk->id;
                        $judul      = $produk->name;
                        //$deskripsi  = $produk->description;
                        $ukuran     = $produk->size;
                        //$harga      = $produk->price;
                        $gambar     = $produk->image;
                        $is_new     = $produk->is_new;
                       ?>

                      <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <?php if($is_new != "0"): ?>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <?php endif; ?>
                              <img class="card-img-top img-card-product-slider img-fluid" src="<?php echo e($gambar); ?>" alt="Card image cap">
                            </div>
                            <div class="card-body pt-3 pl-3 ">
                              <h5 class="card-title mt-0 pb-0 mb-1"><?php echo e($judul); ?></h5>
                              <p class="card-text card-desc-product "><?php echo e($ukuran); ?></p>
                            </div>
                        </div>
                      </div>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                  </div>

            </div>
          </div>
          <a class="carousel-control-prev btn prev-product btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="prev">

             <span class="btn-slide-carousel">
              <i class="fas fa-angle-left"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>

            
          </a>
          <a class="carousel-control-next btn next-product btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="next">
             <span class="btn-slide-carousel">
              <i class="fas fa-angle-right"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>
          </a>
        </div>

        <div class="col-md-12">
          <div class="text-center mt-4">
              <a  href="<?php echo e(url('/product/')); ?>" class="btn btn-lg btn-outline-primary px-4" style="font-size: 14px;"><b> Lihat Semua </b></a>
          </div>
        </div>
       
    </div>

    <hr class="p-0 m-0">

    <div class="container-fluid pt-4 pb-5">
        <h3 class="text-white text-center mb-4">News</h3>


        <div class="carousel slide " data-ride="carousel">
         
          <div class="carousel-inner">
            <div class="col-md-10 offset-md-1 px-5">
                  <div id="owl-our-news" class="owl-carousel owl-theme">

                    <?php $__currentLoopData = $news->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $berita): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php 
                        $id         = $berita->id;
                        $judul      = $berita->title;
                        $tanggal    = $berita->date_created;
                        $deskripsi  = $berita->description;
                        $pembuat    = $berita->user_created;
                        $gambar     = $berita->image;
                       ?>

                      <div class="col item">
                        <div class="card card-news-slider">
                            <img class="card-img-top img-card-product-slider img-fluid" src="<?php echo e($gambar); ?>" alt="Card image cap">
                            <div class="card-body">
                              <p class="card-text mb-2"><small class="text-muted">
                              <?php echo e(Carbon\Carbon::parse($tanggal)->formatLocalized('%d %B %Y')); ?>

                               | <?php echo e($pembuat); ?></small></p>
                              <h5 class="card-title mt-0"><?php echo e($judul); ?></h5>
                                  <p class="card-text card-desc">
                                      <?php echo e($deskripsi); ?>


                                  </p>
                            </div>
                        </div>
                      </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                    
                  </div>

            </div>
          </div>
          <a class="carousel-control-prev btn prev-news btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="prev">

            <span class="btn-slide-carousel">
              <i class="fas fa-angle-left"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>

            <!--<span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>-->
          </a>
          <a class="carousel-control-next btn next-news btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="btn-slide-carousel">
              <i class="fas fa-angle-right"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>
          </a>
        </div>

        <div class="col-md-12">
          <div class="text-center mt-4">
              <a  href="<?php echo e(url('/news/')); ?>" class="btn btn-lg btn-outline-primary px-4" style="font-size: 14px;"><b> Lihat Semua </b></a>
          </div>
        </div>
       
    </div>
  </div>
</main>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>