<?php $__env->startSection('judul_halaman', 'News'); ?>

<?php $__env->startSection('head-konten'); ?>
<style type="text/css">
	.bg-news-page{
		background-image: url("<?php echo e(asset('/images/sample-gambar1.jpg')); ?>");
		background-size: cover;
	}

	.breadcrumb-news {
		background:none !important;
	}

	.breadcrumb-item-news {
		color:#fff !important;
	}

	.breadcrumb-item-news a{
		color:#fff !important;
	}

	.breadcrumb-item-news+.breadcrumb-item-news::before{
		content: ">" !important;
		color:#fff !important;

	}

	.form-contact .form-group input, .form-contact .form-group textarea {
		background-color: transparent;
		border-color:#4A5A80;
		padding: 25px 25px 25px 25px;
	}

	.alert-absolute{
		position: absolute;
		top: 2%;
		z-index: 99999; 
	}

	#moreInfo {
	    position: unset;
	    border-radius: 1em;
	    overflow: hidden;
	    width: 100%;
	}
</style>

<div class="col-md-12 bg-news-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning" style="color:#c39343 !important;">Contact</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('konten'); ?>

<div class="col-md-12 alert-absolute">
	<div class="container" >
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 offset-md-3">

				<?php if($message = Session::get('sukses')): ?>
				<div class="alert alert-success fade show text-center">
					<?php echo e($message); ?>

					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?php endif; ?>

				<?php if($message = Session::get('gagal')): ?>
				<div class="alert alert-danger fade show text-center">
					<?php echo e($message); ?>

					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>


<div class="col-md-12 bg-navy"  style="border-bottom: thin solid  aqua">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-news mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-news"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-news text-capitalize active" aria-current="page">
		    	<?php echo e(Request::segment(1)); ?>

		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="col-md-12 bg-navy">
<div class="container">
	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="row">
			    <div class="col-sm-8 text-white">

					<h5 class="text-white my-4">CONTACT US</h5>

			      	<p>
		        		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		        		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		        	</p>

		        	<form  action="<?php echo e(url('/contact/send')); ?>" class="form-contact" method="POST">
		        		<input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">

					  <div class="form-row ">
					    <div class="form-group col-md-6">
					      <input type="text" name="fName" class="form-control" id="inputEmail4" placeholder="Nama Depan">
					    </div>
					    <div class="form-group col-md-6">
					      <input type="text" name="lName" class="form-control" id="inputEmail4" placeholder="Nama Belakang">
					    </div>
					  </div>
					  <div class="form-group ">
					    <input type="email" name="email"  class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group">
					    <textarea class="form-control" name="message"  id="exampleFormControlTextarea1" rows="9" placeholder="pesan"></textarea>
					  </div>
					  
					  <button type="submit" class="btn btn-warning btn-daftar-sekarang btn-lg px-5">Kirim</button>
					</form>
			    </div>

			    <div class="col-sm-4 text-white">

          			<img class="mb-2 my-4" src="<?php echo e(asset('/images/logo.png')); ?>" alt="" width="200" >

          			<div class=" map-responsive" style="border-radius: 2.5em;  display: flex;">
          				<span id="moreInfo">
          					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.7096489827754!2d112.69552931477494!3d-7.273842994749735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fc1c047fd167%3A0xedde34e8181d4b0f!2sPrimaax!5e0!3m2!1sen!2sid!4v1560233537833!5m2!1sen!2sid" height="275" class="w-100" frameborder="0" style="border:0" allowfullscreen=""></iframe>

					   	</span>

					</div>

					<div class="mt-4">
					<address style="color:#465476">
					   	Address : Ruko Chofa, Jl. Raya Sukomanunggal Jaya, Sukomanunggal, Kec. Sukomanunggal, Kota SBY, Jawa Timur 60188 <br>
					   	<br>
             			Hotline : (031) 7326835<br>
             			<br>
              			Monday-Friday: 08:00 am - 05.00 pm 

					</address>
					</div>
			    </div>
			 </div>

			
		</div>
	</div>

</div>

</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>