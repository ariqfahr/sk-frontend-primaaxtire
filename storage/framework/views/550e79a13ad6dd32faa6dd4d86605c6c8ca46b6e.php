<?php $__env->startSection('judul_halaman', 'Product'); ?>

<?php $__env->startSection('head-konten'); ?>
<style type="text/css">
	.bg-news-page{
		background-image: url("<?php echo e(asset("/images/banner inner page.png")); ?>");
		background-size: cover;
	}

	.breadcrumb-news {
		background:none !important;
	}

	.breadcrumb-item-news {
		color:#fff !important;
	}

	.breadcrumb-item-news a{
		color:#fff !important;
	}

	.breadcrumb-item-news+.breadcrumb-item-news::before{
		content: ">" !important;
		color:#fff !important;

	}

	.list-group-product div{
		background-color: #2D3B5E !important;
		color:#fff;
	}

	.list-group-product div:hover, .list-group-product div:hover a.text-category{
		color: aqua !important;
	}



	.list-group-product a, .list-group-product a:focus{
		color:#fff;
		font-weight: bold;
		text-decoration: none;
	}


	.list-group-product a:hover{
		color: aqua !important;
	}


	.list-group-product-jr li{
		background-color: #2D3B5E !important;
		padding-left:50px !important; 
		font-size:13px;
		color:#fff !important;
		border: none !important;
	}


	.list-group-product-jr a{
		text-decoration: none !important;
	}

	.list-group-product-jr a:hover, .list-group-product-jr a:focus{
		color: aqua !important;
	}


	.accordion-toggle:after {
	    /* symbol for "opening" panels */
	    font-family:'FontAwesome';
	    content:"\f106";
	    float: right;
	    color: inherit;
	}

	.collapsed .accordion-toggle:after {
	    /* symbol for "collapsed" panels */
	    content:"\f107";
	}


	

</style>

<div class="col-md-12 bg-news-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning">Product</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('konten'); ?>


<div class="col-md-12 "  style="border-bottom: thin solid  #202e51">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-news mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-news"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-news text-capitalize active" aria-current="page">
		    	<?php echo e(Request::segment(1)); ?>

		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="container">


    <h3 class="text-white text-left text-sm-left mt-4 text-capitalize">
    	<b> <?php echo e(Request::segment(1)); ?> </b>
    </h3>
	<div class="row">
		<div class="col-sm-12 col-md-3 col-lg-3 ">

			

			<div id="accordion">
				<div class="list-group list-group-product mt-4" id="list-tab" role="tablist">

					<?php $__currentLoopData = $data_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                <?php 
		                    $id         = $value->id;
		                    $nama	    = $value->nama;
		                    $gambar     = $value->gambar;
		                    $sub_cat    = $value->subCategory;
		                 ?>

		            <div class="list-group-item list-group-item-action collapsed " id="list-home-list" data-toggle="collapse" data-target="#collapse<?php echo e($id); ?>" 
		             aria-controls="home"
		             	 <?php if($loop->first): ?>
		             	 	style ="border-top-left-radius: 20px; border-top-right-radius: 20px;"
		             	 <?php endif; ?>

		             	<?php if($loop->last): ?>
		             	 	style ="border-bottom-left-radius: 20px; border-bottom-right-radius: 20px;"
		             	<?php endif; ?>

		             >
		             	<?php
		             		if (!empty($_GET['product_category_id'])) {
		             			$product_category_id = $_GET['product_category_id'];
		             		} else {
		             			$product_category_id = "";	
		             		}

		             		if (!empty($_GET['id'])) {
		             			$product_id = $_GET['id'];
		             		} else {
		             			$product_id = "";	
		             		}
		             	?>
				      	<a class="text-category" href=" <?php echo e(url("/product?product_category_id=$id&page=1")); ?> "
				      		<?php if(($product_category_id==$id) || ($product_id==$id)): ?>
				      			style= " color:aqua !important; " 
				      		<?php endif; ?>
				      	>

				      	<?php echo e($nama); ?></a>
					
						<span class="accordion-toggle float-right pt-1" 
							<?php if(($product_category_id==$id) || ($product_id==$id)): ?>
				      			style= " color:aqua !important; " 
				      		<?php endif; ?>
				      		>
					   		<!--<i class="fas fa-angle-down fa-lg"></i>-->
					    </span>
						
						
				    </div>

				    <div id="collapse<?php echo e($id); ?>" class="collapse

				    	<?php if($product_id==$id): ?>
				      		show
				      	<?php endif; ?>

				    " aria-labelledby="headingTwo" data-parent="#accordion">
					      	<ul class="list-group list-group-flush list-group-product-jr">
					      		<?php $__currentLoopData = $sub_cat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						      		<?php 
					                    $sub_id         = $value2->sub_id;
				    	                $sub_nama	    = $value2->sub_nama;
				        	            $sub_gambar     = $value2->sub_gambar;
				        	          ?>

					      			<li class="list-group-item">
									 	<a href=" <?php echo e(url("/product?id=$id&product_category_id=$sub_id")); ?> "
									 	<?php if(($product_category_id==$sub_id)): ?>
							      			style= " color:aqua !important; " 
							      		<?php endif; ?>

									 	><?php echo e($sub_nama); ?></a>
									</li>

		            			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

								
							</ul>
					</div>



		            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		            

				</div>
			</div>

			
		</div>

		<div class="col-sm-12 col-md-9 col-lg-9 p-0 pb-4">

			<div class="row m-0">

			<?php if(!empty($product->data)): ?>
				<?php if($product->message == "success"): ?>


		            <?php $__currentLoopData = $product->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                    <?php 
	                        $id         = $value->id;
	                        $judul      = $value->code;
	                        $nama    	= $value->name;
	                        $ukuran	  	= $value->size;
	                        $deskripsi  = $value->description;
	                        $harga    	= $value->price;
	                        $gambar     = $value->image;
	                        $is_new     = $value->is_new;
						 ?>

						<div class="col-6  col-sm-6 col-md-3 mt-4">
		                <div class="card card-product-slider">
                            <div>
                              <?php if($is_new != "0"): ?>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <?php endif; ?>
                              <img class="card-img-top img-card-product-slider img-fluid" src="<?php echo e($gambar); ?>" alt="Card image cap">
                            </div>
                            <div class="card-body pt-3 pl-3 ">
                              <h5 class="card-title mt-0 pb-0 mb-1"><?php echo e($judul); ?></h5>
                              <p class="card-text card-desc-product "><?php echo e($ukuran); ?></p>
                            </div>
                        </div>
		            </div>


					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					<div class="col-md-12 mt-4">
					
				<div class="float-right text-white">

					<nav aria-label="Page navigation example">
					  <ul class="pagination justify-content-end">

					  	<?php
					  		$css_disable = "  ";

			             	if(!empty($_GET["page"]) && ($_GET["page"] != 0)){
								$prev_id = $_GET["page"] - 1;
								$page_now = $_GET["page"];
								$next_id  = $page_now + 1;

							} else if (!empty($_GET["page"]) && ($_GET["page"] == 0)) {
								$prev_id  = $_GET["page"];
								$page_now = $_GET["page"] + 1;
								$next_id  = $page_now + 1;
							}
							else{
					    		$prev_id = 0;
								$page_now = 1;
								$next_id  = $page_now + 1;

								$css_disable = " disabled ";
							}

							if(!empty($_GET["product_category_id"])){
						  		$product_category_id = $_GET["product_category_id"];
						  		$url_prev = url("/product?product_category_id=$product_category_id&page=$prev_id");
						  		$url_next = url("/product?product_category_id=$product_category_id&page=$next_id");
							} else{

						  		$url_prev = url("/product?page=$prev_id");
						  		$url_next = url("/product?page=$next_id");
							}

						

			            ?>


					   	<?php if($product->pages > 1): ?> 

					    <li class="page-item 
					    	<?php 
					    		echo $css_disable
					    	 ?>
					    	"
					    >
					      <a class="page-link" 
					      	href="<?php echo e($url_prev); ?>" 

					      	<?php 
					    		echo $css_disable
					    	 ?>

					      tabindex="-1">Previous</a>
					    </li>

						<?php endif; ?> 


					   	<?php if($product->pages > 1): ?>
						<?php for($i = 1; $i <= $product->pages; $i++): ?>
							<li class="page-item 
								<?php if($i == $page_now): ?>
									active
								<?php endif; ?> 
							">
								<a class="page-link" 
					      			href="<?php echo e(url("/product?page=$i")); ?>" >
									<?php echo e($i); ?>

								</a>
							</li>

						<?php endfor; ?>

						<?php endif; ?> 

					   	<?php if(($product->pages > 1) && ($product->pages < $page_now)  ): ?> 
							<li class="page-item">
						      <a class="page-link" href="<?php echo e($url_next); ?>" >Next</a>
						    </li>
						<?php endif; ?> 

					    
					  </ul>
					</nav>
					

				</div><br>

				</div>
				

					<!--<div class="col-md-12 mt-4">
					
					<div class="float-right text-white">Pagination disini</div><br>

					</div>-->

					<br>

		        <?php else: ?>
		        	<div class="alert alert-warning col-12  col-sm-12 col-md-12 mt-4" role="alert">
					  Data tidak ditemukan. 
					</div>
		        <?php endif; ?>

				

		        <?php

				/*@for ($i = 1; $i <= 12; $i++)
				    <div class="col-6  col-sm-6 col-md-3 mt-4">
		                <div class="card card-product-slider">
		                    <div>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
		                </div>
		            </div>

				@endfor*/

				?>

			<?php else: ?>

				<div class="col-12  col-sm-12 mt-4">
					<div class="alert alert-info fade show text-center">
						Data tidak ditemukan.
						
					</div>
				</div>

			<?php endif; ?>

			</div>
		</div>

	</div>

			
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>