<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route blog
Route::get('/', 'HomeController@home');


Route::get('/home', 'HomeController@home');


Route::get('/slider', 'HomeController@slider');


Route::get('/news', 'NewsController@news');


Route::get('/product', 'ProductController@product');


Route::get('/partner', 'PartnerController@partner');
Route::get('/partner/get-kota', 'PartnerController@cariKota');
Route::get('/partner/get-nama', 'PartnerController@autoNamaToko');
Route::get('/partner/get-by-name', 'PartnerController@cariNama');
Route::get('/partner/get-by-auto', 'PartnerController@cariId');
Route::get('/partner/get-by-near', 'PartnerController@cariTerdekat');

Route::get('/partner/ex/', 'PartnerController@partner_ex');

Route::get('/about', 'AboutController@about');


Route::get('/contact', 'ContactController@contact');
Route::post('/contact/send', 'ContactController@sending');
Route::get('/contact/get-pesan', 'ContactController@pesan');


Route::get('/reseller', 'ResellerController@reseller');
