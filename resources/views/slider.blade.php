<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<title>Grid Column Carousel - Designify.me</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }} ">
<!--https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css-->
<link rel="stylesheet" href="{{ asset('/css/style.css') }}">

<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<!-- Custom styles for this template -->
<link href="{{ asset('/css/main.css') }} " rel="stylesheet" type="text/css">
<link href="{{ asset('/css/GridColumnCarousel.min.css') }} " rel="stylesheet" type="text/css">
    
<script src="{{ asset('/js/GridColumnCarousel.js') }} "></script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="">Grid Column Carousel</a> </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="">Back to Designify</a></li>
      </ul>
    </div>
    <!--/.navbar-collapse --> 
  </div>
</nav>


      <div class="container">
      
      <div class="row">
<div class="col-md-12 text-center">
  <h2 class="mrgn-50">Grid Column Carousel</h2>
  <hr/>
  </div>
  </div>
      
      <div class="grid-column-carousel row">
        <ul class="grid-column-carousel__list">
          <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
         
            <h3>Noluisse facilisis est et, in vis brute altera malorum.</h3>
            <p> Mel tantas animal scaevola ex, maiorum erroribus in vel, mutat tation te vim. Erant tantas accusamus vim an. Cu eos hendrerit scribentur. Nam ad scripta volumus, hinc deserunt ut eum, mei tota principes constituto ex.</p>
        
          </li>
          <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
            <h3>Noluisse facilisis est et, in vis brute altera malorum.</h3>
            <p> Mel tantas animal scaevola ex, maiorum erroribus in vel, mutat tation te vim. Erant tantas accusamus vim an. Cu eos hendrerit scribentur. Nam ad scripta volumus, hinc deserunt ut eum, mei tota principes constituto ex.</p>
          </li>
          <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
             <h3>Noluisse facilisis est et, in vis brute altera malorum.</h3>
            <p> Mel tantas animal scaevola ex, maiorum erroribus in vel, mutat tation te vim. Erant tantas accusamus vim an. Cu eos hendrerit scribentur. Nam ad scripta volumus, hinc deserunt ut eum, mei tota principes constituto ex.</p>
          </li>
          <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
             <h3>Noluisse facilisis est et, in vis brute altera malorum.</h3>
            <p> Mel tantas animal scaevola ex, maiorum erroribus in vel, mutat tation te vim. Erant tantas accusamus vim an. Cu eos hendrerit scribentur. Nam ad scripta volumus, hinc deserunt ut eum, mei tota principes constituto ex.</p>
          </li>
          <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
             <h3>Noluisse facilisis est et, in vis brute altera malorum.</h3>
            <p> Mel tantas animal scaevola ex, maiorum erroribus in vel, mutat tation te vim. Erant tantas accusamus vim an. Cu eos hendrerit scribentur. Nam ad scripta volumus, hinc deserunt ut eum, mei tota principes constituto ex.</p>
          </li>
          <li class="col-xs-12 col-sm-6 col-md-4 col-lg-4 ">
             <h3>Noluisse facilisis est et, in vis brute altera malorum.</h3>
            <p> Mel tantas animal scaevola ex, maiorum erroribus in vel, mutat tation te vim. Erant tantas accusamus vim an. Cu eos hendrerit scribentur. Nam ad scripta volumus, hinc deserunt ut eum, mei tota principes constituto ex.</p>
          </li>
        </ul>
        <div class="grid-column-carousel__page-indicators">
          <ul>
              <li class="dot active"></li>
          </ul>
        </div>
      </div>
      
      
      
       
    
      
      <!-- Button section to demonstrate the methods that can be called on the GCCarousel object -->
      <div class="button-panel">
        <button class="first btn btn-success btn-sm">Go to First</button>
        <button class="prev btn btn-success btn-sm">Go to Prev</button>
        <button class="next btn btn-success btn-sm">Go to Next</button>
        <button class="last btn btn-success btn-sm">Go to Last</button>
      </div>
      
</div>
  
  
  <footer>
    <p style="margin-top:80px;" class="text-center">Created with <span class="icon"><i class="fa fa-heart fa-lg"></i></span> by <a href="http://designify.me/" target="_blank"> Designify.me</a></p>
  </footer>
</div>
 <script>
    window.onload = function() {
      var options = {
        elem: document.getElementsByClassName('grid-column-carousel')[0],
        gridColClasses: 'col-xs-12 col-sm-6 col-md-4 col-lg-4',
        autoplay: true
      };
       
      var gCCarousel = new GCCarousel(options);
      
      /* Demonstrate the different methods that can be called on the GCCarousel object */
      document.querySelector('button.first').addEventListener('click', function() {
        gCCarousel.slide('first');
      });
      document.querySelector('button.prev').addEventListener('click', function() {
        gCCarousel.slide('prev');
      });
      document.querySelector('button.next').addEventListener('click', function() {
        gCCarousel.slide('next');
      });
      document.querySelector('button.last').addEventListener('click', function() {
        gCCarousel.slide('last');
      });
    };
  </script>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="{{ asset('/js/bootstrap.min.js') }} "></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
