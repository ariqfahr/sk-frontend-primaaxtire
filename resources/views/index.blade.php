<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" href="{{ asset('/images/ikon.jpg') }}" type="image/gif" sizes="16x16">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }} ">
   <!--https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css-->
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/owlcarousel/assets/owl.theme.default.min.css') }}">


    <title>Primaax</title>
  </head>
  <body style="background-color: #0D162C;">

    <style type="text/css">
      .card-product-slider{
        background-color: #2D3B5E;
        border-width: 0px;
        color:#fff;
        border-radius: 20px;
      }

      .card-product-slider img{
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
      }

      .card-product-slider .card-body .card-desc{
        font-size: 13px;
      }

      .card-product-slider .card-body .card-desc-product{
        font-size: 14px;
      }

      .img-card-product-slider{
        height: 200px;
        object-fit: cover;
      }


      .btn-owl-slider{
        box-shadow: none !important;
      }

      .btn-slide-carousel{
          border: thin solid aqua;
          border-radius: 200px;
          padding:20px 25px;
      }

      .btn-slide-carousel i{
        color: aqua !important;
        font-size:25px;
      }

      .carousel-indicator-dots{
        width: 10px !important;
        height: 10px !important;
        border-radius: 100%;
      }

      .btn-daftar-sekarang{
        background-color: #C39343;
        font-weight: 600;
        color:#fff;
        border-color: #2D344A;
      }


    </style>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primaax py-0">
      <div class="container ">
        <a class="navbar-brand" href="#">
          <img src="{{ asset('/images/logo.png') }}" width="150" class="d-inline-block align-top" alt="">
         
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerMenu" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerMenu">
          <ul class="navbar-nav navbar-menu mt-lg-0 ml-auto ">
            <li class="nav-item active mx-2 py-2">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item mx-2 py-2">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item mx-2 py-2">
              <a class="nav-link" href="#">Product</a>
            </li>
            <li class="nav-item mx-2 py-2">
              <a class="nav-link" href="#">Partner</a>
            </li>
            <li class="nav-item mx-2 py-2">
              <a class="nav-link" href="#">Reseller</a>
            </li>
            <li class="nav-item mx-2 py-2">
              <a class="nav-link" href="#">News</a>
            </li>
            <li class="nav-item mx-2 py-2">
              <a class="nav-link" href="#">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

<header>

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="carousel-indicator-dots active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1" class="carousel-indicator-dots" ></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2" class="carousel-indicator-dots" ></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item item-background active" style="background-image: url('{{ asset("/images/slide1.jpg") }}')">
          <div class="carousel-caption d-none d-md-block">
            <h5>...</h5>
            <p>...</p>
          </div>

      </div>
      <div class="carousel-item item-background"  style="background-image: url('{{ asset("/images/slide2.jpg") }}')">
        <!--<img class="d-block w-100" src="..." alt="Second slide">-->
      </div>
      <div class="carousel-item item-background"  style="background-image: url('{{ asset("/images/slide3.jpg") }}')">
        <!--<img class="d-block w-100" src="..." alt="Third slide">-->
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="col-md-12 bg-navy div-gplay py-2">
    <div class="d-flex justify-content-center">
      <a href="">
        <img src="{{ asset('/images/google-play-badge.png') }}" style='width:200px;'>
      </a>
    </div>
  </div>
</header>

<main>
  <div class="container">
    <div class="row py-4">
      <div class="col-md-6">
        <img src="{{ asset('/images/sample-gambar1.jpg') }}" class="rounded float-left w-100" alt="...">
      </div>
      <div class="col-md-6 text-white">
        <h2 class='text-white'>About Us</h2>
        <p class="my-4">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>

        <a href="" class="h5 text-info">
          Read More
        </a>
      </div>
    </div>
  </div>

   
  <div class="col-md-12 bg-navy div-gplay">
    <div class="container-fluid py-4">
        <h3 class="text-white text-center mb-4">Our Product</h3>


        <div class="carousel slide " data-ride="carousel">
         
          <div class="carousel-inner">
            <div class="col-md-10 offset-md-1 px-5">
                  <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 

                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                        <div class="card card-product-slider">
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
                        </div>
                                 
                    </div>
                  </div>

            </div>
          </div>
          <a class="carousel-control-prev btn prev-product btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="prev">

             <span class="btn-slide-carousel">
              <i class="fas fa-angle-left"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>

            
          </a>
          <a class="carousel-control-next btn next-product btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="next">
             <span class="btn-slide-carousel">
              <i class="fas fa-angle-right"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>
          </a>
        </div>

        <div class="col-md-12">
          <div class="text-center mt-4">
              <button type="button" class="btn btn-lg btn-outline-primary"><b> Lihat Semua </b></button>
          </div>
        </div>
       
    </div>

    <hr class="p-0 m-0">

    <div class="container-fluid pt-4 pb-5">
        <h3 class="text-white text-center mb-4">News</h3>


        <div class="carousel slide " data-ride="carousel">
         
          <div class="carousel-inner">
            <div class="col-md-10 offset-md-1 px-5">
                  <div id="owl-our-news" class="owl-carousel owl-theme">
                    <div class="col item">
                        <div class="card card-product-slider">
                            <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/news1.jpg') }}" alt="Card image cap">
                            <div class="card-body">
                              <p class="card-text mb-2"><small class="text-muted">2 Maret 2019</small></p>
                              <h5 class="card-title mt-0">Lorem ipsum dolor sit amet</h5>
                                  <p class="card-text card-desc">
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                      tempor incididunt ut labore et dolore magna aliqua. 

                                  </p>
                            </div>
                        </div>
                                 

                    </div>
                    <div class="col item">
                      <div class="card card-product-slider">
                            <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/news1.jpg') }}" alt="Card image cap">
                              <div class="card-body">
                              <p class="card-text mb-2"><small class="text-muted">2 Maret 2019</small></p>
                              <h5 class="card-title mt-0">Lorem ipsum dolor sit amet</h5>
                                  <p class="card-text card-desc">
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                      tempor incididunt ut labore et dolore magna aliqua. 

                                  </p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                      <div class="card card-product-slider">
                            <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/news1.jpg') }}" alt="Card image cap">
                              <div class="card-body">
                              <p class="card-text mb-2"><small class="text-muted">2 Maret 2019</small></p>
                              <h5 class="card-title mt-0">Lorem ipsum dolor sit amet</h5>
                                  <p class="card-text card-desc">
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                      tempor incididunt ut labore et dolore magna aliqua. 

                                  </p>
                            </div>
                        </div>
                                 
                    </div>
                    <div class="col item">
                      <div class="card card-product-slider">
                            <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/news1.jpg') }}" alt="Card image cap">
                              <div class="card-body">
                              <p class="card-text mb-2"><small class="text-muted">2 Maret 2019</small></p>
                              <h5 class="card-title mt-0">Lorem ipsum dolor sit amet</h5>
                                  <p class="card-text card-desc">
                                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                      tempor incididunt ut labore et dolore magna aliqua. 

                                  </p>
                            </div>
                        </div>
                                 
                    </div>
                  </div>

            </div>
          </div>
          <a class="carousel-control-prev btn prev-news btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="prev">

            <span class="btn-slide-carousel">
              <i class="fas fa-angle-left"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>

            <!--<span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>-->
          </a>
          <a class="carousel-control-next btn next-news btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="btn-slide-carousel">
              <i class="fas fa-angle-right"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>
          </a>
        </div>

        <div class="col-md-12">
          <div class="text-center mt-4">
              <button type="button" class="btn btn-lg btn-outline-primary"><b> Lihat Semua </b></button>
          </div>
        </div>
       
    </div>

   
       
  </div>

    <div class="container py-5">
      <div class="row py-4">
        <div class="col-md-5">
          <center>
            <img src="{{ asset('/images/contoh-lingkaran.png') }}" class="rounded w-90" alt="...">
          </center>
        </div>
        <div class="col-md-7 text-white">
          <h2 class='text-white mt-4'>Become a Reseller & Droshipper</h2>
          <p class="my-4">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>

          <a href="" class="btn btn-warning btn-daftar-sekarang px-4 py-2">
            Daftar Sekarang!
          </a>
        </div>
      </div>
    </div>


    <div class="container mb-5">
      <div class="col-md-12 " style="background-color: #2D3B5E;  border-radius: 20px 20px 20px 0px; ">
        <div class="row">
          <div class="col-md-4 pl-0 pt-0 " style="height:295px;">
              <picture  style="position:relative; margin-left: -3px !important; top:-80px !important;">
                <img src="{{ asset('/images/footer-hp.png') }}" class="img-fluid img-thumbnail bg-transparent border-0 " >
              </picture>

          </div>
        
          <div class="col-md-8  text-center align-self-center text-white " style="padding-top:20px;">
            <div class="padding-top: -20px;">
              <p class="h4">
                  Download Aplikasinya sekarang juga !
              </p>
              <div class="d-flex justify-content-center">
              <a href="">
                <img src="{{ asset('/images/google-play-badge.png') }}" style='width:200px;'>
              </a>
            </div>
           
          </div>
          </div>
        </div>
      </div>
  </div>

</main>


<div class="col-md-12 bg-navy div-gplay" style="background-color: #2D3B5E">
  <div class="container">
    <footer class="pt-4 my-md-5 pt-md-5 ">
      <div class="row">
        <div class="col-12 col-md-6">
          <img class="mb-2" src="{{ asset('/images/logo.png') }}" alt="" width="150" >
          <ul class="list-unstyled text-small">
            <li><a class="text-muted" href="#">
              Address : 34 Fordham Street, New York, NY 10027
            </a></li>
            <li><a class="text-muted" href="#">
              Hotline : + (012) 3456-7890; (012) 3456-7891;
            </a></li>
            <li><a class="text-muted" href="#">
              Monday-Friday: 09:00 am - 06.00 pm, Satuday : 09.00 am - 04.00 pm
            </a></li>
          </ul>
        </div>
        <div class="col-6 col-md-3">
          <h5 class='text-white'>Menu</h5>
          <ul class="list-unstyled text-small" style="columns: 2; -webkit-columns: 2; -moz-columns: 2;">
            <li><a class="text-muted" href="#">Home</a></li>
            <li><a class="text-muted" href="#">About</a></li>
            <li><a class="text-muted" href="#">Product</a></li>
            <li><a class="text-muted" href="#">Partner</a></li>
            <li><a class="text-muted" href="#">Reseller</a></li>
            <li><a class="text-muted" href="#">News</a></li>
            <li><a class="text-muted" href="#">Contacts</a></li>
          </ul>
        </div>
        <div class="col-6 col-md-3">
          <h5 class='text-white'>Follow Us</h5>
          <ul class="list-group list-group-horizontal">
            <li class="list-group-item">
              <i class="fab fa-facebook-f"></i>
            </li>
            <li class="list-group-item">
              <i class="fab fa-instagram"></i>
            </li>
            <li class="list-group-item">
              <i class="fab fa-whatsapp"></i>
            </li>
            <li class="list-group-item">
              <i class="fab fa-twitter"></i>
            </li>
          </ul>
        </div>
       
      </div>
    </footer>

    


</div>

<hr>

<div class="col-md-12 bg-navy div-gplay" style="background-color: #2D3B5E">
    <div class="container">
      <div class="row">
        <div class="col-md-9 ">
            <p class="d-block mb-3 text-muted">© 2019 PRIMAAX. All Rights Reserved</p>
        </div>
        <div class="col-md-3 ml-auto">

          <div class="row">
            <div class="col">
              <a href="">Privacy Policy</a>
            </div>
            <div class="col">
              <a href="">Refund Policy</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
</div>

    <script src="{{ asset('/js/jquery-3.3.1.slim.min.js') }}" ></script>
    <script src="{{ asset('/js/popper.min.js') }} " ></script>
    <script src="{{ asset('/js/bootstrap.min.js') }} "></script>


    <script src="{{ asset('/owlcarousel/owl.carousel.js') }} "></script>

    <script>
            $(document).ready(function() {


              var owl_our_news = $("#owl-our-news");
              owl_our_news.owlCarousel({
                  responsiveClass:true,
                  responsive:{
                      0:{
                          items:1,
                          nav:false,
                          dots:false
                      },
                      600:{
                          items:2,
                          nav:false,
                          dots:false
                      },
                      1000:{
                          items:2,
                          nav:false,
                          loop:false,
                          dots:false
                      }
                  }
              });

               // Custom Navigation Events
              $(".next-news").click(function(){
                owl_our_news.trigger('next.owl.carousel');
              })
              $(".prev-news").click(function(){
                owl_our_news.trigger('prev.owl.carousel');
              })




              var owl = $("#owl-demo");

              owl.owlCarousel({
                  responsiveClass:true,
                  responsive:{
                      0:{
                          items:1,
                          nav:false,
                          dots:false
                      },
                      600:{
                          items:3,
                          nav:false,
                          dots:false
                      },
                      1000:{
                          items:5,
                          nav:false,
                          loop:false,
                          dots:false
                      }
                  }
              });




             
              // Custom Navigation Events
              $(".next-product").click(function(){
                owl.trigger('next.owl.carousel');
              })
              $(".prev-product").click(function(){
                owl.trigger('prev.owl.carousel');
              })
              $(".play").click(function(){
                owl.trigger('play.owl.carousel',1000); //owl.play event accept autoPlay speed as second parameter
              })
              $(".stop").click(function(){
                owl.trigger('stopss.owl.carousel');
              })

            })
          </script>

    <script type="text/javascript">
      $(document).ready(function(){
        // Activate Carousel
        $("#our-product").carousel({interval: false});
      });
    
    </script>

  </body>
</html>