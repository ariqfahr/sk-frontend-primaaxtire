@extends('master')

@section('judul_halaman', 'reseller')

@section('head-konten')


<div class="col-md-12 bg-reseller-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning">About</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

@endsection

@section('konten')


<div class="col-md-12 bg-navy"  style="border-bottom: thin solid  aqua">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-reseller mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-reseller"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-reseller text-capitalize active" aria-current="page">
		    	{{ Request::segment(1) }}
		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="col-md-12 bg-navy">
<div class="container">
	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="row">
			    <div class="col-sm-8 offset-md-2 text-white pt-5">

			      	<p class="text-center px-5 mb-5" style="color: #8790A5; font-size: 15px;">
		        		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		        		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		        	</p>

		       
			    </div>


			    <div class="col-sm-8 offset-md-2 text-white col-form-reseller">
			    	<form class="form-reseller">

					  <h5>Data Diri</h5>

					  <div class="form-row ">
					    <div class="form-group col-md-6">
					      <input type="text" class="form-control" id="inputEmail4" placeholder="Nama Depan">
					    </div>
					    <div class="form-group col-md-6">
					      <input type="text" class="form-control" id="inputEmail4" placeholder="Nama Belakang">
					    </div>

						  <div class="form-group col-md-12">
						    <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
						  </div>
					  </div>

					  <h5>FOTO KTP</h5>

					  <div class="form-row ">
					    <div class="custom-file">
						    <input type="file" class="custom-file-input" id="inputGroupFile01">
						    <label class="custom-file-label" for="inputGroupFile01">
						    	Pilih file
						    </label>
						</div>
					  </div>

					  <br>
					  
					  <h5>INFORMASI ALAMAT</h5>

					  <div class="form-row ">

					    <div class="form-group col-md-6">
					      	<select class="custom-select custom-icon-select" id="selectProvinsi">
							  <option selected>Provinsi</option>
							  <option value="1">DKI Jakarta</option>
							  <option value="2">Jawa Barat</option>
							  <option value="3">Bali</option>
							</select>
							<label class="custom-select-icon" for="selectProvinsi">
						    </label>

					    </div>
					    <div class="form-group col-md-6">
					      	<select class="custom-select custom-icon-select" id="selectKota">
							  <option selected>Kota</option>
							  <option value="1">Bandung</option>
							  <option value="2">Jakarta</option>
							  <option value="3">Surabaya</option>
							</select>
							<label class="custom-select-icon" for="selectKota">
						    </label>
					      	
					    </div>
					  </div>


					  	<div class="form-row ">

						  <div class="form-group col-md-12">
						    <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" placeholder="Alamat Lengkap"></textarea>
						  </div>
						 </div>


					  	<div class="form-row ">
						  <div class="form-group col-md-6">
						  	<input type="text" class="form-control" id="inputEmail4" placeholder="No. Hp ">
						  </div>
						  <div class="form-group col-md-6">

						  	<input type="text" class="form-control" id="inputEmail4" placeholder="Kode pos">
						  </div>
						
						</div>

						<center>
							  <button type="submit" class="btn btn-warning btn-daftar-sekarang btn-lg px-5 mt-2">Daftar</button>
						  	
						</center>
					</form>
			    </div>

			 </div>

			
		</div>
	</div>

</div>

</div>

@endsection