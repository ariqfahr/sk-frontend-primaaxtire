@extends('master')

@section('judul_halaman', 'About')

@section('head-konten')


<div class="col-md-12 bg-about-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning" style="color:#c39343 !important;">About</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

@endsection

@section('konten')


<div class="col-md-12 bg-navy"  style="border-bottom: thin solid  aqua">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-about mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-about"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-about text-capitalize active" aria-current="page">
		    	{{ Request::segment(1) }}
		    </li>
		  </ol>
		</nav>
	</div>
</div>



@foreach($about->data as $value)
      
        	@if($value->id == "2") 
        	  	@php
		          $id_about         = $value->id;
		          $judul_about       = $value->title;
		          $deskripsi_about   = $value->description;
		          $gambar_about      = $value->image;

         		@endphp

	        @elseif($value->id == "3") 
	        	@php
		        
		        	$id_mission        = $value->id;
			        $judul_mission       = $value->title;
			        $deskripsi_mission   = $value->description;
			        $gambar_mission      = $value->image;
			    @endphp
		    @else
		    	@php
		        	$id_goals       = $value->id;
			        $judul_goals      = $value->title;
			        $deskripsi_goals  = $value->description;
			        $gambar_goals     = $value->image;

		        @endphp
		        
	        @endif
@endforeach



<div class="col-md-12 bg-navy">
<div class="container">
	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12">
			<h4 class="text-white my-4">
				<b> About us </b>
			</h4>
			<div class="row">
			    <div class="col-sm-6 order-sm-12 mb-4">
			      <img src="{{ $gambar_about }}" alt="" class="rounded w-100"  />
			    </div>
			    <div class="col-sm-6 order-sm-1 text-white text-justify">
			      	<p>
			      		{{ $deskripsi_about }}
					</p>
			    </div>
			  </div>

			
		</div>
	</div>

</div>

</div>

<div class="col-md-12 ">
    <div class="container py-4">

		<div class="row text-justify">
			<div class="col-md-6 text-white ">
				<h3 class="my-4">Our Goals</h3>
				<p>
		        		{{ $deskripsi_goals }}
		        		<br>
		        </p>
			</div>

			<div class="col-md-6 text-white ">

				<h3 class="my-4">Our Mission</h3>
				<p>
		        		
		        		{{ $deskripsi_mission }}
		        		<br>
		        </p>
			</div>

		</div>

    </div>
</div>
@endsection