@extends('master')

@section('judul_halaman', 'Product')

@section('head-konten')


<div class="col-md-12 bg-product-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning" style="color:#c39343 !important;">Product</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

@endsection

@section('konten')


<div class="col-md-12 "  style="border-bottom: thin solid  #202e51">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-product mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-product"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-product text-capitalize active" aria-current="page">
		    	{{ Request::segment(1) }}
		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="container">
    <h4 class="text-white text-left text-sm-left mt-4 text-capitalize">
    	<b> {{ Request::segment(1) }} </b>
    </h4>
	<div class="row">
		<div class="col-sm-12 col-md-3 col-lg-3 ">
			<div id="accordion">
				<div class="card list-group list-group-product mt-4" id="list-tab" role="tablist">

					@foreach($data_category as $value)
		                @php
		                    $id         = $value->id;
		                    $nama	    = $value->nama;
		                    $gambar     = $value->gambar;
		                    $sub_cat    = $value->subCategory;
		                @endphp

		            <div class="list-group-item list-group-item-action collapsed " id="list-home-list" data-toggle="collapse" data-target="#collapse{{ $id }}" 
		             aria-controls="home" onclick="return false;">
		             	<?php
		             		if (!empty($_GET['product_category_id'])) {
		             			$product_category_id = $_GET['product_category_id'];
		             		} else {
		             			$product_category_id = "";	
		             		}

		             		if (!empty($_GET['id'])) {
		             			$product_id = $_GET['id'];
		             		} else {
		             			$product_id = "";	
		             		}
		             	?>
				      	<a class="text-category" href=" {{ url("/product?product_category_id=$id&page=1") }} "
				      		@if (($product_category_id==$id) || ($product_id==$id))
				      			style= " color:aqua !important; " 
				      		@endif
				      	>{{ $nama }}</a>
					
						<span class="accordion-toggle float-right pt-1" 
							@if (($product_category_id==$id) || ($product_id==$id))
				      			style= " color:aqua !important; " 
				      		@endif
				      		>
					   		<!--<i class="fas fa-angle-down fa-lg"></i>-->
					    </span>
						
						
				    </div>

				    <div id="collapse{{ $id }}" class="collapse

				    	@if ($product_id==$id)
				      		show
				      	@endif

				    " aria-labelledby="headingTwo" data-parent="#accordion">
					      	<ul class="list-group list-group-flush list-group-product-jr">
					      		@foreach($sub_cat as $value2)
						      		@php
					                    $sub_id         = $value2->sub_id;
				    	                $sub_nama	    = $value2->sub_nama;
				        	            $sub_gambar     = $value2->sub_gambar;
				        	         @endphp

					      			<li class="list-group-item">
									 	<a href=" {{ url("/product?id=$id&product_category_id=$sub_id") }} "
									 	@if (($product_category_id==$sub_id))
							      			style= " color:aqua !important; " 
							      		@endif

									 	>{{ $sub_nama }}</a>
									</li>

		            			@endforeach

								
							</ul>
					</div>



		            @endforeach
		            

				</div>
			</div>

			
		</div>

		<div class="col-sm-12 col-md-9 col-lg-9 p-0 pb-4">

			<div class="row m-0">

			@if (!empty($product->data))
				@if ($product->message == "success")


		            @foreach($product->data as $value)
	                    @php
	                        $id         = $value->id;
	                        $judul      = $value->code;
	                        $nama    	= $value->name;
	                        $ukuran	  	= $value->size;
	                        $deskripsi  = $value->description;
	                        $harga    	= $value->price;
	                        $gambar     = $value->image;
	                        $is_new     = $value->is_new;
							if (!empty($value->image_web)){
								$gambar     = $value->image_web;
							}
						@endphp

						<div class="col-6  col-sm-6 col-md-3 mt-4">
		                <div class="card card-product-slider">
                              @if ($is_new != "0")
                              		<div class="ribbon-wrapper">
										<div class="ribbon red">New</div>
									</div>
                              @endif
                            <div>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{  $gambar }}" alt="Card image cap">
                            </div>
                            <div class="card-body pt-3 pl-3 ">
                              <h5 class="card-title mt-0 pb-0 mb-1">{{  $judul  }}</h5>
                              <p class="card-text card-desc-product ">{{  $ukuran }}</p>
                            </div>
                        </div>
		            </div>


					@endforeach

					<div class="col-md-12 mt-4">
					
				<div class="float-right text-white">

					<nav aria-label="Page navigation example">
					  <ul class="pagination justify-content-end">

					  	<?php
					  		$css_disable = "  ";

			             	if(!empty($_GET["page"]) && ($_GET["page"] != 0)){
								$prev_id = $_GET["page"] - 1;
								$page_now = $_GET["page"];
								$next_id  = $page_now + 1;

							} else if (!empty($_GET["page"]) && ($_GET["page"] == 0)) {
								$prev_id  = $_GET["page"];
								$page_now = $_GET["page"] + 1;
								$next_id  = $page_now + 1;
							}
							else{
					    		$prev_id = 0;
								$page_now = 1;
								$next_id  = $page_now + 1;

								$css_disable = " disabled ";
							}

							if(!empty($_GET["product_category_id"])){
						  		$product_category_id = $_GET["product_category_id"];
						  		$url_prev = url("/product?product_category_id=$product_category_id&page=$prev_id");
						  		$url_next = url("/product?product_category_id=$product_category_id&page=$next_id");
							} else{

						  		$url_prev = url("/product?page=$prev_id");
						  		$url_next = url("/product?page=$next_id");
							}

						

			            ?>


					   	@if($product->pages > 1) 

					    <li class="page-item 
					    	@php
					    		echo $css_disable
					    	@endphp
					    	"
					    >
					      <a class="page-link" 
					      	href="{{ $url_prev }}" 

					      	@php
					    		echo $css_disable
					    	@endphp

					      tabindex="-1">Previous</a>
					    </li>

						@endif 


					   	@if($product->pages > 1)
						@for ($i = 1; $i <= $product->pages; $i++)
							<li class="page-item 
								@if ($i == $page_now)
									active
								@endif 
							">
								<a class="page-link" 
					      			href="{{ url("/product?page=$i") }}" >
									{{ $i }}
								</a>
							</li>

						@endfor

						@endif 

					   	@if(($product->pages > 1) && ($product->pages < $page_now)  ) 
							<li class="page-item">
						      <a class="page-link" href="{{ $url_next }}" >Next</a>
						    </li>
						@endif 

					    
					  </ul>
					</nav>
					

				</div><br>

				</div>
				

					<!--<div class="col-md-12 mt-4">
					
					<div class="float-right text-white">Pagination disini</div><br>

					</div>-->

					<br>

		        @else
		        	<div class="alert alert-warning col-12  col-sm-12 col-md-12 mt-4" role="alert">
					  Data tidak ditemukan. 
					</div>
		        @endif

				

		        <?php

				/*@for ($i = 1; $i <= 12; $i++)
				    <div class="col-6  col-sm-6 col-md-3 mt-4">
		                <div class="card card-product-slider">
		                    <div>
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{ asset('/images/produk1.jpg') }}" alt="Card image cap">
                            </div>
                            <div class="card-body">
                              <h5 class="card-title mt-0">Storm Rider</h5>
                                  <p class="card-text card-desc-product">Tersedia 6 ukuran.</p>
                            </div>
		                </div>
		            </div>

				@endfor*/

				?>

			@else

				<div class="col-12  col-sm-12 mt-4">
					<div class="alert alert-info fade show text-center">
						Data tidak ditemukan.
						
					</div>
				</div>

			@endif

			</div>
		</div>

	</div>

			
</div>
@endsection