@extends('master')

@section('judul_halaman', 'Home')

@section('head-konten')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">

      @php
        $urutan = 0;    
      @endphp

      @foreach($slider->data as $slide)

        <li data-target="#carouselExampleIndicators" 
          data-slide-to="{{$urutan++}}" 
        class="carousel-indicator-dots active">
         
        </li>

        

      @endforeach
      <!--<li data-target="#carouselExampleIndicators" data-slide-to="1" class="carousel-indicator-dots" ></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2" class="carousel-indicator-dots" ></li>-->
    </ol>
    <div class="carousel-inner">

      @foreach($slider->data as $slide)
          

      <div class="carousel-item item-background active" >
          <img src="{{ $slide->image }}"  class="d-block w-100">
          <?php  
            /*<div class="carousel-caption d-none d-md-block">
              <h5>


              </h5>
              <p>...</p>
            </div>*/

          ?>

      </div>


      @endforeach

    </div>

  </div>

  <div class="col-md-12 bg-navy py-2">
    <div class="d-flex justify-content-center">
      <a href="">
        <img src="{{ asset('/images/google-play-badge.png') }}" style='width:200px;'>
      </a>
    </div>
  </div>
  
@endsection


@section('konten')
<main>

<div class="col-md-12">

<div class="container-fluid pt-4 pb-5">
        <h3 class="text-white text-center mb-4">Gallery</h3>
        <div class="carousel slide " data-ride="carousel">
         
          <div class="carousel-inner main-gallery">
            <div class="col-md-10 offset-md-1 px-5">
                  <div id="owl-our-gall" class="owl-carousel owl-theme">
                  

                    @foreach($gallery->data as $gal)
                      @php
                        $id         = $gal->id;
                        $judul      = $gal->title;
                        $deskripsi  = $gal->description;
                        $gambar     = $gal->image;
                      @endphp

                      <div class="col item" style="">
                        <div class="card-gall-slider px-0" style="height:430px; ">
                        <a href="{{ $gambar }}" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-12">
                            <img class="card-img-top  img-fluid" src="{{ $gambar }}" alt="Card image cap" style="">
                          </a>
                      
                        </div>
                        
                      </div>

                    @endforeach
                    
                    
                  </div>

            </div>
          </div>
          <a class="carousel-control-prev btn prev-gall btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="prev">

            <span class="btn-slide-carousel">
              <i class="fas fa-angle-left"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>

            <!--<span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>-->
          </a>
          <a class="carousel-control-next btn next-gall btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="btn-slide-carousel">
              <i class="fas fa-angle-right"></i>
              <!--<span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>-->
            </span>
          </a>
        </div>
       
    </div>

  
</div>

  
  <div class="col-md-12 bg-navy div-gplay">
    <div class="container-fluid py-4">
        <h3 class="text-white text-center mb-4">New Product</h3>


        <div class="carousel slide " data-ride="carousel">
         
          <div class="carousel-inner">
            <div class="col-md-10 offset-md-1 px-5">
                  <div id="owl-demo" class="owl-carousel owl-theme">
                    @foreach($product->data as $produk)
                      @php
                        $id         = $produk->id;
                        $judul      = $produk->name;
                        //$deskripsi  = $produk->description;
                        $ukuran     = $produk->size;
                        //$harga      = $produk->price;
                        $gambar     = $produk->image;
                        $is_new     = $produk->is_new;
                      @endphp

                      <div class="col item" style="margin-top:5px;">
                        <div class="card card-product-slider">
                            
                          <div class="ribbon-wrapper">
                              <div class="ribbon red">New</div>
                          </div>
                            <div>
                              <?php 
                              /*@if ($is_new != "0")
                              <h5 style="position:absolute;">
                                  <span class="badge badge-secondary badge-danger px-4 py-1" style="border-top-left-radius: 20px;">Baru</span>
                              </h5>
                              @endif */
                              ?>
                              <img class="card-img-top img-card-product-slider img-fluid" src="{{  $gambar }}" alt="Card image cap">
                            </div>
                            <div class="card-body pt-3 pl-3 text-justify">
                              <h5 class="card-title mt-0 pb-0 mb-1">{{  $judul  }}</h5>
                              <p class="card-text card-desc-product ">{{  $ukuran }}</p>
                            </div>
                        </div>

                      </div>

                    @endforeach


                  </div>

            </div>
          </div>
          <a class="carousel-control-prev btn prev-product btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="prev">

             <span class="btn-slide-carousel">
              <i class="fas fa-angle-left"></i>
            </span>

            
          </a>
          <a class="carousel-control-next btn next-product btn-owl-slider" role="button" data-slide="next">
             <span class="btn-slide-carousel">
              <i class="fas fa-angle-right"></i>
            </span>
          </a>
        </div>

        <div class="col-md-12">
          <div class="text-center mt-4">
              <a  href="{{ url('/product/') }}" class="btn btn-lg btn-outline-primary px-4" style="font-size: 14px;"><b> Lihat Semua </b></a>
          </div>
        </div>
       
    </div>

    <hr class="p-0 m-0">

    <div class="container-fluid pt-4 pb-5">
        <h3 class="text-white text-center mb-4">News</h3>


        <div class="carousel slide " data-ride="carousel">
         
          <div class="carousel-inner">
            <div class="col-md-10 offset-md-1 px-5">
                  <div id="owl-our-news" class="owl-carousel owl-theme">

                    @foreach($news->data as $berita)
                      @php
                        $id         = $berita->id;
                        $judul      = $berita->title;
                        $tanggal    = $berita->date_created;
                        $deskripsi  = $berita->description;
                        $pembuat    = $berita->user_created;
                        $gambar     = $berita->image;
                      @endphp

                      <div class="col item">
                        <div class="card card-news-slider">
                            <img class="card-img-top img-card-product-slider img-fluid" src="{{ $gambar }}" alt="Card image cap">
                            <div class="card-body text-justify">
                              <p class="card-text mb-2"><small class="text-muted">
                              {{ Carbon\Carbon::parse($tanggal)->formatLocalized('%d %B %Y')}}
                               | {{ $pembuat }}</small></p>
                              <h5 class="card-title mt-0">{{ $judul }}</h5>
                                  <p class="card-text card-desc">
                                      {{ $deskripsi }}

                                  </p>
                            </div>
                        </div>
                      </div>
                    @endforeach
                    
                    
                  </div>

            </div>
          </div>
          <a class="carousel-control-prev btn prev-news btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="prev">

            <span class="btn-slide-carousel">
              <i class="fas fa-angle-left"></i>
            </span>
          </a>
          <a class="carousel-control-next btn next-news btn-owl-slider" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="btn-slide-carousel">
              <i class="fas fa-angle-right"></i>
            </span>
          </a>
        </div>

        <div class="col-md-12">
          <div class="text-center mt-4">
              <a  href="{{ url('/news/') }}" class="btn btn-lg btn-outline-primary px-4" style="font-size: 14px;"><b> Lihat Semua </b></a>
          </div>
        </div>
       
    </div>
  </div>
</main>

@endsection




