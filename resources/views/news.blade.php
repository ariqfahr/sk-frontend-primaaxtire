@extends('master')

@section('judul_halaman', 'News')

@section('head-konten')
<style type="text/css">
	.bg-news-page{
		background-image: url("{{ asset("/images/banner inner page.png") }}");
		background-size: cover;
	}

	.breadcrumb-news {
		background:none !important;
	}

	.breadcrumb-item-news {
		color:#fff !important;
	}

	.breadcrumb-item-news a{
		color:#fff !important;
	}

	.breadcrumb-item-news+.breadcrumb-item-news::before{
		content: ">" !important;
		color:#fff !important;

	}
</style>

<div class="col-md-12 bg-news-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning" style="color:#c39343 !important;">NEWS</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

@endsection

@section('konten')


<div class="col-md-12 "  style="border-bottom: thin solid  #202e51">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-news mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-news"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-news text-capitalize active" aria-current="page">
		    	{{ Request::segment(1) }}
		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="container">
	<div class="row">

		<div class="col-sm-12 col-md-12 col-lg-12 p-0">
			<div class="row m-0">
				@foreach($news->data as $berita)
                      @php
                        $id         = $berita->id;
                        $judul      = $berita->title;
                        $tanggal    = $berita->date_created;
                        $deskripsi  = $berita->description;
                        $pembuat    = $berita->user_created;
                        $gambar     = $berita->image;
                @endphp


				    <div class="col-6  col-sm-6 col-md-6 mt-4">
		                <div class="card card-news-slider">
		                    <img class="card-img-top img-card-product-slider img-fluid" src="{{ $gambar }}" alt="Card image cap">
		                    <div class="card-body">
		                        <p class="card-text mb-2"><small class="text-muted"> {{ Carbon\Carbon::parse($tanggal)->formatLocalized('%d %B %Y')}} | {{ $pembuat }}</small></p>
		                        <h5 class="card-title mt-0">{{ $judul }}</h5>
		                            <p class="card-text card-desc">
		                                {{ $deskripsi }}
				 	                </p>
		                    </div>
		                </div>
		            </div>


				@endforeach

				<div class="col-md-12 mt-4">
					
				<div class="float-right text-white">

					<nav aria-label="Page navigation example">
					  <ul class="pagination justify-content-end">

					  	<?php
			             	if(!empty($_GET["page"]) && ($_GET["page"] != 0)){
								$prev_id = $_GET["page"] - 1;
								$page_now = $_GET["page"];
								$next_id  = $page_now + 1;

							} else if (!empty($_GET["page"]) && ($_GET["page"] == 0)) {
								$prev_id  = $_GET["page"];
								$page_now = $_GET["page"] + 1;
								$next_id  = $page_now + 1;
							}
							else{
					    		$prev_id = 0;
								$page_now = 1;
								$next_id  = $page_now + 1;

								$css_disable = " disabled ";
							}
			            ?>


					   	@if($news->pages > 1) 

					    <li class="page-item 
					    	@php
					    		echo $css_disable
					    	@endphp
					    	"
					    >
					      <a class="page-link" 
					      	href="{{ url('/news?page=$prev_id') }}" 

					      	@php
					    		echo $css_disable
					    	@endphp

					      tabindex="-1">Previous</a>
					    </li>

						@endif 


					   	@if($news->pages > 1)
						@for ($i = 1; $i <= $news->pages; $i++)
							<li class="page-item 
								@if ($i == $page_now)
									active
								@endif 
							">
								<a class="page-link" href="#">
									{{ $i }}
								</a>
							</li>

						@endfor
						
						@endif 

					   	@if($news->pages > 1) 
							<li class="page-item">
						      <a class="page-link" href="#">Next</a>
						    </li>
						@endif 

					    
					  </ul>
					</nav>
					

				</div><br>

				</div>
				


			</div>
		</div>
	</div>

			
</div>
@endsection