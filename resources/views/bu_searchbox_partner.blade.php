@extends('master')

@section('judul_halaman', 'News')

@section('head-konten')
<style type="text/css">
	.bg-news-page{
		background-image: url("{{ asset("/images/banner inner page.png") }}");
		background-size: cover;
	}

	.breadcrumb-news {
		background:none !important;
	}

	.breadcrumb-item-news {
		color:#fff !important;
	}

	.breadcrumb-item-news a{
		color:#fff !important;
	}

	.breadcrumb-item-news+.breadcrumb-item-news::before{
		content: ">" !important;
		color:#fff !important;

	}

	.list-group-product a, .list-group-product a:focus{
		background-color: #2D3B5E !important;
		color:#fff;
		font-weight: bold;
	}


	.list-group-product a:hover{
		color: aqua !important;
	}


	.list-group-product-jr li{
		background-color: #2D3B5E !important;
		padding-left:50px !important; 
		font-size:13px;
		color:#fff !important;
		border: none !important;
	}


	.list-group-product-jr a{
		text-decoration: none !important;
	}

	.list-group-product-jr a:hover, .list-group-product-jr a:focus{
		color: #395B98 !important;
	}


	.accordion-toggle:after {
	    /* symbol for "opening" panels */
	    font-family:'FontAwesome';
	    content:"\f106";
	    float: right;
	    color: inherit;
	}

	.collapsed .accordion-toggle:after {
	    /* symbol for "collapsed" panels */
	    content:"\f107";
	}

	.map-container-2{
		overflow:hidden;
		padding-bottom:56.25%;
		position:relative;
		height:0;
	}
	.map-container-2 iframe{
		left:0;
		top:0;
		height:100%;
		width:100%;
		position:absolute;
	}

	.btn-lokasi{
		background-color: #C39343;
		border:none !important;
		color:#fff;
	}

	.form-partner{
		background-color: transparent !important;
		border:thin solid #fff !important;
	}

	#infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
       display: none;
    }

    #map-canvas #infowindow-content {
        display: inline;
    }
	

</style>

<div class="col-md-12 bg-news-page">
	<div class="container" >
		<div class="row " style="height: 150px">
		   <div class="col-sm-12 my-auto">
		   		<center>
		     			<h2 class="text-warning">Partner</h2>
		     	</center>
		   </div>
		</div>
  	</div>
</div>

@endsection

@section('konten')


<div class="col-md-12 "  style="border-bottom: thin solid  aqua">
	<div class="container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-news mb-0 pl-0">
		    <li class="breadcrumb-item breadcrumb-item-news"><a href="#">Home</a></li>
		    <li class="breadcrumb-item breadcrumb-item-news text-capitalize active" aria-current="page">
		    	{{ Request::segment(1) }}
		    </li>
		  </ol>
		</nav>
	</div>
</div>


<div class="container">


    <h1 class="text-white text-left text-sm-left mt-4 text-capitalize">{{ Request::segment(1) }}</h1>

    <div class="row ">
		<div class="col-md-3 pt-2">
			<a class="btn btn-warning btn-lokasi btn-lg px-4 w-100" style="height:65px; padding-top: 20px;"
			//href="{{ url("/partner?searchBy=dekat") }}"
			onclick="initMap()";
			>
				Lokasi Terdekat Saya
			</a>
		</div>
		
		<div class="col-md-3 pt-2">
			<input class="form-control form-control-lg form-partner px-4" type="text" placeholder="Cari Berdasarkan Kota" style="height:65px;" id="searchCities">
		</div>

  		<div class="col-md-4 pt-2 ml-auto">
  			<div class="input-group mb-3">
			  <input type="text" class="form-control form-partner" placeholder="Cari nama" aria-label="Recipient's username" aria-describedby="basic-addon2" style="height:65px; font-size: 1.25em">
			  <div class="input-group-append">
			    <button class="btn btn-outline-light" type="button">
			    	<i class="fas fa-search"></i>
			    </button>
			  </div>
			</div>
  		</div>


					
	</div>

	<div class="row m-0 pt-4">

		@foreach($lokasi->data as $val)
            @php
                $id         = $val->id;
                $nama     	= $val->name;
                $latitude   = $val->latitude;
                $longitude  = $val->longitude;
        	@endphp
        @endforeach

		<input type="hidden" name="id" id="idMaps" value="<?php echo $id ?>">
		<input type="hidden" name="nama" id="namaMaps" value="<?php echo $nama ?>">
		<input type="hidden" name="lat" id="latlng" value="<?php echo $latitude.",".$longitude ?>">

			<?php
				if(!empty($_GET["searchBy"])){
					$page_id = $_GET["searchBy"] ;
				} else{

		    		$page_id = 0;
				}
			?>
			<!--Google map-->
			<div class="col-sm-12 col-md-12 col-lg-12 z-depth-1-half map-container-2" style="height: 500px" id="map-canvas">
			  <!--<iframe class="" src="https://maps.google.com/maps?q=chicago&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" style="border:0" allowfullscreen></iframe>-->
			</div>

			<div id="infowindow-content">
		      <img src="" width="16" height="16" id="place-icon">
		      <span id="place-name"  class="title"></span><br>
		      <span id="place-address"></span>
		    </div>

			<!--Google Maps-->

	</div>

			
</div>
@endsection


@section('js-konten')



<script>
    // memanggil library Geocoder
   // var geocoder = new google.maps.Geocoder();
	
	// memanggil library Infowindow untuk memunculkan infowindow pada marker
	//var infowindow = new google.maps.InfoWindow();
	var marker;

	var map;

	function initMap() {   

		var input = document.getElementById('latlng').value;

		var latlngStr = input.split(',', 2);
		var latlng = new google.maps.LatLng(latlngStr[0], latlngStr[1]);


		var myLatLng = {lat: latlngStr[0], lng: latlngStr[1]};

            //alert(myLatLng);
            var Lating = new google.maps.LatLng(latlngStr[0], latlngStr[1]),
            options = {
            	center: Lating,
            	zoom: 12
            },
            map = new google.maps.Map(document.getElementById("map-canvas"), options);


                    
            var marker = new google.maps.Marker({
              position: Lating,
              map: map,
              title: 'Primaax Tire',
              label: "Toko Primaax 1",
              
              // setting latitude & longitude as title of the marker
              // title is shown when you hover over the marker
              //title: latlngStr[1] + ', ' + latlngStr[1]
            });            
        }

    function initMapByName() {   

		var input = document.getElementById('searchCities').value;

		var latlngStr = input.split(',', 2);
		var latlng = new google.maps.LatLng(latlngStr[0], latlngStr[1]);

		var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        
        /*var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });*/


		var myLatLng = {lat: latlngStr[0], lng: latlngStr[1]};

            //alert(myLatLng);
        var Lating = new google.maps.LatLng(latlngStr[0], latlngStr[1]),
            options = {
            	center: Lating,
            	zoom: 12
            },
        
        map = new google.maps.Map(document.getElementById("map-canvas"), options);
		
		var marker = new google.maps.Marker({
              position: Lating,
              map: map,
			  anchorPoint: new google.maps.Point(0, -29),
			  title: 'Primaax Tire',
              label: "Toko Primaax 1",
              
              // setting latitude & longitude as title of the marker
              // title is shown when you hover over the marker
              //title: latlngStr[1] + ', ' + latlngStr[1]
            });            
        }



    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('searchCities');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
 
	// kita munculkan peta default 
	/*function initialize() {


  		map = new google.maps.Map(document.getElementById('map-canvas'), {
        	zoom: 12,
        	// posisi default ada di kota Bandung
        	center: {lat: -7.273843, lng: 112.6955293}
      	});
	}*/
 
	/*function codeLatLng() {
		// ambil value dari combobox


   

		var input = document.getElementById('latlng').value;

		var latlngStr = input.split(',', 2);
		var latlng = new google.maps.LatLng(latlngStr[0], latlngStr[1]);


		var myLatLng = {lat: latlngStr[0], lng: latlngStr[1]};
            
        map = new google.maps.Map(document.getElementById('map'), {
              center: myLatLng,
              zoom: 14                    
            });

		// cari lokasi dari latitude dan longitude
		geocoder.geocode({'location': map}, function(results, status) {

			if (status == google.maps.GeocoderStatus.OK) {
				;
				// jika berhasil, map akan secara automatis berpindah ke koordinat tersebut
				if (results[1]) {
			    	map.setZoom(13);
			    	map.setCenter(map)
			    	marker = new google.maps.Marker({
			      		position: map,
			      		map: map
			    	});
			    	infowindow.setContent(results[1].formatted_address);
			    	infowindow.open(map, marker);

			  	} else {
			    	window.alert('No results found');
			  	}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
	}
 
	google.maps.event.addDomListener(window, 'load', initialize);*/
    </script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCv200mEGfbuiFnGWWXRvi8UATWwzszGu0&callback=initMap&libraries=places&region=ID" 
type="text/javascript"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>

 
<script type="text/javascript">
    var route = "{{ url('/partner/get-kota') }}";
    var $input = $("#searchCities");


    $input.typeahead({
	   source:  function (term, process) {
    	
        return $.get(route, 
        	{ term: term }, 
        		function (data) {
        			//console.log(data);
					//alert(JSON.stringify(data));
                	return process(data);
            	}
            );
        } ,
	  autoSelect: true
	});

    
	$input.change(function() {
	  var current = $input.typeahead("getActive");
	  if (current) {
	    // Some item from your model is active!
	    if (current.name == $input.val()) {
	    	alert("klik");
	      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
	    } else {
	    	//alert("ga ada");
	      // This means it is only a partial match, you can either add a new item
	      // or take the active if you don't want new items
	    }
	  } else {
	    // Nothing is active so it is a new value (or maybe empty value)
	  }
	});

	/* untuk source di atas
	$('#q').typeahead({
        source:  function (term, process) {
    	
        return $.get(route, 
        	{ term: term }, 
        		function (data) {
        			//console.log(data);
					//alert(JSON.stringify(data));
                	return process(data);
            	}
            );
        }
    });*/
  
</script>

@endsection
