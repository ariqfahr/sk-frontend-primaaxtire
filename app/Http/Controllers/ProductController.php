<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class ProductController extends Controller
{
	public function index(){
    	return view('product');
    }

    public function product(){

    	$client = New Client();

    	if(!empty($_GET["product_category_id"])){
    		$category_id = $_GET["product_category_id"] ;
			if(!empty($_GET["page"])){
				$page = $_GET["page"] - 1;
			} else {
				$page = 0 ;
			}
		} else {
			if(!empty($_GET["page"])){
				$page = $_GET["page"] - 1;
			} else {
				$page = 0 ;
			}
		}

    	if (!empty($category_id)) {
    		$requestProduct     = $client->get("https://api-dev.primaax.co.id/v2/product?product_category_id=$category_id&page=$page");
    	} else{
        	$requestProduct     = $client->get("https://api-dev.primaax.co.id/v2/product/category?page=$page");
    	}

		$response['product']	= json_decode($requestProduct->getBody()->getContents());

		//print_r($response['product']);

        $requestSosmed      = $client->get('https://api-dev.primaax.co.id/v2/cms/sosmed');
        $requestCategory    = $client->get('https://api-dev.primaax.co.id/v2/category');

        $response['sosmed'] 	= json_decode($requestSosmed->getBody()->getContents());
        $response['category'] 	= json_decode($requestCategory->getBody()->getContents());


		$response['data_category'] = array();

       /* $array = Array (
			    "0" => Array (
			        "id" => "USR1",
			        "name" => "Steve Jobs",
			        "company" => "Apple",
			        "sub" => Array (
			        	"no" => "1",
			        	"nama" => "2"
			        )
			    )
			);

        $json = json_encode(array('data' => $array));

        $print = json_decode($json);
		//print_r($print);



        $json1ToArray = (array) $requestCategory;

       

        $newCategory = array();
        */

        $no = 0;
        foreach($response['category']->data as $value){
		    $id         = $value->id;
		    $nama	    = $value->name;
		    $gambar     = $value->image;


	        $requestSubCategory    	= $client->get("https://api-dev.primaax.co.id/v2/product/category?category_id=$id");

	        $res_subcategory 		= json_decode($requestSubCategory->getBody()->getContents());

		    $array_baru = Array (
			        "id" => $value->id,
			        "nama" => $value->name,
			        "gambar" => $value->image,
			        "subCategory" => array()
			);

		    foreach($res_subcategory->data as $value2){
		    	$id2         = $value2->id;
			    $nama2	     = $value2->name;
			    $gambar2     = $value2->image;

			    $array_baru2 = Array (
			    	"sub_id" => $id2,
			    	"sub_nama" => $nama2,
			    	"sub_gambar" => $gambar2
			    );
			    array_push($array_baru["subCategory"], $array_baru2);
		    }




		    array_push($response['data_category'], $array_baru);

		   
        	

			$no++;


		}

		$response['data_category'] = json_encode($response['data_category']);

		$response['data_category']= json_decode($response['data_category']);

		//print($response['data_category']);

		//var_dump($response['data_category']);

		/*foreach($response['data_category'] as $item) {
			echo $item->id;
		}*/


		return view('product', $response);
    }


}