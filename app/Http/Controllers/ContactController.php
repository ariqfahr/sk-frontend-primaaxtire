<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Session;

class ContactController extends Controller
{
	public function index(){
    	return view('contact');
    }

    public function contact(){

    	$client = New Client();
        $requestSosmed      = $client->get('https://api-dev.primaax.co.id/v2/cms/sosmed');

        $response['sosmed'] = json_decode($requestSosmed->getBody()->getContents());
        
    	return view('contact', $response);
    }



    public function sending(Request $request){

    	$client = New Client();

    	$url 		= "https://api-dev.primaax.co.id/v2/cms/contact";

    	$fName 		= $request->input('fName');
    	$lName 		= $request->input('lName');
     	$email 		= $request->input('email');
    	$message 	= $request->input('message');


    	$response = $client->request('POST', $url, [
		    'form_params' => [
		        'first_name' =>$fName,
				'last_name'=>$lName,
				'email'=>$email,
				'message'=>$message
		    ]
		]);

	    /*$request  = $client->post($url,  ['first_name'=>$fName, 'last_name'=>$lName, 'email'=>$email, 'message'=>$message]);
	    $response = $request->send();*/
	  
	    //dd($response);

	    Session::flash('sukses',"
	    	Terimakasih $fName $lName atas pesan dan masukannya. 
	    	");

	    return redirect('/contact');


    }

}