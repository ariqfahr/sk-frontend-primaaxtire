<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class PartnerController extends Controller
{
	public function index(){
    	return view('partner');
    }

    public function partner(){

    	$client = New Client();
        $requestSosmed      = $client->get('https://api-dev.primaax.co.id/v2/cms/sosmed');
        $response['sosmed'] = json_decode($requestSosmed->getBody()->getContents());

    	$requestLoc   	= $client->get("https://api-dev.primaax.co.id/v2/cms/partners");
    	$response['lokasi'] = json_decode($requestLoc->getBody()->getContents());
    		
    	$url 		= $client->get("https://api-dev.primaax.co.id/v2/cms/cities");
		$response['cariKota'] = json_decode($url->getBody()->getContents());

		/*echo count($response['cariKota']->data);
		//print_r($response['cariKota']->data);

		for ($i=1; $i <= count($response['cariKota']->data); $i++) { 
		    echo ($response['cariKota']->data[$i]->city);
			$myObj->kota =  $response['cariKota']->data[$i]->city;

		    echo ($myObj->kota);
		}*/

		//print_r($myJSON);


        	

    	return view('partner_ex', $response);
    }

    public function cariKota(Request $request){

    	$client = New Client();

    	$url 		= $client->get("https://api-dev.primaax.co.id/v2/cms/cities");
		$response['cariKota'] = json_decode($url->getBody()->getContents());

		/*foreach ($response['cariKota']->data as $value) {
		    $myObj->kota =  $value->city;
		    echo ($value->city);
		}

		$myJSON = json_encode($myObj);
		*/

		$myObj = array(array());
		$no = 0;

		//print_r($response['cariKota']);

		$myObj["kode"]  = $response['cariKota']->meta->code;
		$myObj["pesan"]  = $response['cariKota']->meta->message;

		//echo $myObj["kode"] ;
		
		foreach ($response['cariKota']->data as $value) {
					//echo ($value->city);
				    $myObj[$no]["id"] =  $value->id;
				    $myObj[$no]["kota"] =  $value->city;
				    $no++;
		}
		
		


		

		$myJSON = json_encode($myObj);
    	
	    return response()->json($myObj);


    }

    public function autoNamaToko(Request $request){

    	$client = New Client();

    	$url 		= $client->get("https://api-dev.primaax.co.id/v2/cms/stores/");
		$response['hasil'] = json_decode($url->getBody()->getContents());


		$myObj = array(array());
		$no = 0;
		foreach ($response['hasil']->data as $value) {
			//echo ($value->city);
		    $myObj[$no]["id"] =  $value->id;
		    $myObj[$no]["name"] =  $value->name;
		    $no++;
		}

		$myJSON = json_encode($myObj);
    	
	    return response()->json($myObj);


    }

    public function cariNama(Request $request){

    	$client = New Client();


    	$url 		= $client->get("https://api-dev.primaax.co.id/v2/cms/partners?store=".$_GET['store']);
		$response['hasil'] = json_decode($url->getBody()->getContents());

		//echo "https://api-dev.primaax.co.id/v2/cms/partners?store=".$_GET['store'];

		$myObj = array(array());
		$no = 0;

		//print_r($response['cariKota']);

		$myObj["status"]["kode"]  = $response['hasil']->meta->code;
		$myObj["status"]["pesan"]  = $response['hasil']->meta->message;

		//echo $myObj["kode"] ;
		
		foreach ($response['hasil']->data as $value) {
					//echo ($value->city);
				    $myObj["value"][$no]["id"] =  $value->id;
				    $myObj["value"][$no]["name"] =  $value->name;
				    $myObj["value"][$no]["latitude"] =  $value->latitude;
				    $myObj["value"][$no]["longitude"] =  $value->longitude;
				    $no++;
		}
		
		


		

		$myJSON = json_encode($myObj);
    	


		//$myJSON = json_encode($response['hasil']->data);


    	//print_r(response()->json($myJSON));
	    
	    return response()->json($myJSON);


    }

    public function cariId(Request $request){

    	$client = New Client();


    	$url 		= $client->get("https://api-dev.primaax.co.id/v2/cms/partners?city=".$_GET['city']);
		$response['hasil'] = json_decode($url->getBody()->getContents());

		//echo "https://api-dev.primaax.co.id/v2/cms/partners?store=".$_GET['store'];

		$myJSON = json_encode($response['hasil']->data);


    	//print_r(response()->json(json_encode($myJSON)));
	    
	    return response()->json($myJSON);


    }

    public function cariTerdekat(Request $request){

    	$client = New Client();


    	$url 		= $client->get("https://api-dev.primaax.co.id/v2/cms/partners?latitude=".$_GET['latitude']."&longitude=".$_GET['longitude']);
		$response['hasil'] = json_decode($url->getBody()->getContents());

		//echo "https://api-dev.primaax.co.id/v2/cms/partners?store=".$_GET['store'];

		$myJSON = json_encode($response['hasil']->data);


		//$myObj["kode"]  = $response['hasil']->meta->code;
		//$myObj["pesan"]  = $response['hasil']->meta->message;

    	//print_r(response()->json(json_encode($myJSON)));
	    
	    return response()->json($myJSON);


    }

    public function partner_ex(){

    	$client = New Client();
        $requestSosmed      = $client->get('https://api-dev.primaax.co.id/v2/cms/sosmed');
        $response['sosmed'] = json_decode($requestSosmed->getBody()->getContents());

        //"https://api-dev.primaax.co.id/v2/cms/partners?latitude=-7.273843&longitude=112.6955293

    	$requestLoc   	= $client->get("https://api-dev.primaax.co.id/v2/cms/partners");
    	$response['lokasi'] = json_decode($requestLoc->getBody()->getContents());
    		
    	$url 		= $client->get("https://api-dev.primaax.co.id/v2/cms/cities");
		$response['cariKota'] = json_decode($url->getBody()->getContents());

		/*echo count($response['cariKota']->data);
		//print_r($response['cariKota']->data);

		for ($i=1; $i <= count($response['cariKota']->data); $i++) { 
		    echo ($response['cariKota']->data[$i]->city);
			$myObj->kota =  $response['cariKota']->data[$i]->city;

		    echo ($myObj->kota);
		}*/

		//print_r($myJSON);


        	

    	return view('partner_ex', $response);
    }

}