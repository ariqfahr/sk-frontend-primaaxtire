<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class NewsController extends Controller
{
	public function index(){
    	return view('news');
    }

    public function news(){
    	$client = New Client();


    	if(!empty($_GET["page"])){

    		$page_id = $_GET["page"] ;
		} else{

    		$page_id = 0;
		}

    	if (!empty($category_id)) {
    		$requestNews    	= $client->get("https://api-dev.primaax.co.id/v2/cms/news?page=$page_id");
    	} else{

        	$requestNews    	= $client->get("https://api-dev.primaax.co.id/v2/cms/news?page=$page_id");
    	}


        //$requestNews   		= $client->get('https://api-dev.primaax.co.id/v2/cms/news');

        $response['news']= json_decode($requestNews->getBody()->getContents());
       

        $requestSosmed      = $client->get('https://api-dev.primaax.co.id/v2/cms/sosmed');

        $response['sosmed'] = json_decode($requestSosmed->getBody()->getContents());


    	return view('news', $response);
    }
}