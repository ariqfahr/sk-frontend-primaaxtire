<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class AboutController extends Controller
{
	public function index(){
    	return view('about');
    }

    public function about(){
    	$client = New Client();

        $requestAbout    = $client->get('https://api-dev.primaax.co.id/v2/cms/about');

        $requestSosmed      = $client->get('https://api-dev.primaax.co.id/v2/cms/sosmed');

        $response['about']= json_decode($requestAbout->getBody()->getContents());
        
        $response['sosmed'] = json_decode($requestSosmed->getBody()->getContents());

    	return view('about', $response);
    }
}