<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;


class MenuController extends Controller
{
    public function index(){
    	return view('index');
    }

   

    public function home(){
    	return view('home');
    }


    public function about(){
		return view('about');
	}
 
	public function product(){
		return view('product');
	}
 
	public function partner(){
		return view('partner');
	}

	public function reseller(){
		return view('reseller');
	}
 
	public function contact(){
		return view('contact');
	}
 
	public function news(Request $request){

		$myItems = array(
		    "spider-man" => array(
		        "tanggal" 	=> "02 Maret 2019",
		        "judul" 	=> "Berita 1",
		        "deskripsi" => "lorem ipsum 1",
		    ),
		    "super-man" => array(
		        "tanggal" 	=> "02 Maret 2019",
		        "judul" 	=> "Berita 2",
		        "deskripsi" => "lorem ipsum 2",
		    ),
		    "iron-man" => array(
		        "tanggal" 	=> "02 Maret 2019",
		        "judul" 	=> "Berita 3",
		        "deskripsi" => "lorem ipsum 3",
		    ),
		    "super-man" => array(
		        "tanggal" 	=> "02 Maret 2019",
		        "judul" 	=> "Berita 4",
		        "deskripsi" => "lorem ipsum 4",
		    )
		);

		$page = $request->get('page', 1);
	    $perPage = 15;
	    $items = collect($myItems);

		

		/*
		$data = array();

		//Get current page form url e.g. &page=6
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($superheroes);

        //Define how many items we want to be visible in each page
        $per_page = 5;

        //Slice the collection to get the items to display in current page
        $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();

        //Create our paginator and add it to the data array
        $data['results'] = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);

        //Set base url for pagination links to follow e.g custom/url?page=6
        $data['results']->setPath($request->url());
        */

		return view('news');
	}
}
