<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class HomeController extends Controller
{
	public function index(){
		$client = New Client();
		$request['slider']	= $client->get('https://api-dev.primaax.co.id/v2/cms/slider'); 
        $request['news']    = $client->get('https://api-dev.primaax.co.id/v2/cms/home/news');
        $request['product'] = $client->get('https://api-dev.primaax.co.id/v2/cms/home/product');
        $request['about']   = $client->get('https://api-dev.primaax.co.id/v2/cms/home/about');
    	$response			= $request->getBody()->getContents();

    	$dataHome			= json_decode($response, true);

    	print("<pre>".print_r($dataHome, true)."</pre>");
    	
    	//return view('home');
    }

    public function home(){

    	$client = New Client();
		$requestSlider		= $client->get('https://api-dev.primaax.co.id/v2/cms/slider'); 
    	$requestNews		= $client->get('https://api-dev.primaax.co.id/v2/cms/news');
        $requestProduct     = $client->get('https://api-dev.primaax.co.id/v2/cms/home/product');
        $requestAbout       = $client->get('https://api-dev.primaax.co.id/v2/cms/home/about');
        $requestGallery     = $client->get('https://api-dev.primaax.co.id/v2/cms/gallery');

        $requestSosmed      = $client->get('https://api-dev.primaax.co.id/v2/cms/sosmed');


    	$response['slider']	= json_decode($requestSlider->getBody()->getContents());
    	$response['news']	= json_decode($requestNews->getBody()->getContents());
        $response['product']= json_decode($requestProduct->getBody()->getContents());
        $response['about']  = json_decode($requestAbout->getBody()->getContents());
        $response['sosmed'] = json_decode($requestSosmed->getBody()->getContents());
        $response['gallery'] = json_decode($requestGallery->getBody()->getContents());

    	$dataHome			= json_encode($response);

		
    	//print("<pre>".print_r( json_encode($response['gallery']), true)." aa</pre>");
    	//print("<pre>".print_r(json_decode($dataHome), true)."</pre>");
    	
    	return view('home', $response);
    }

    public function slider(){
    	return view('slider');
    }
}